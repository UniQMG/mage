package uniqmg.mage;

import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.mage.caster.MagiPlayer;
import uniqmg.mage.commands.CommandBind;
import uniqmg.mage.commands.CommandCast;
import uniqmg.mage.commands.CommandInfuse;
import uniqmg.mage.commands.CommandSetMana;
import uniqmg.mage.util.protection.WorldGuardIntegration;

import java.util.Map;

public final class Mage extends JavaPlugin {
    public final MagiManager magiManager = new MagiManager();
    private BukkitRunnable tick;
    CommandBind commandBind;

    public final NamespacedKey mana = new NamespacedKey(this, "mana");
    public final NamespacedKey maxMana = new NamespacedKey(this, "maxMana");
    public final NamespacedKey maxManaRate = new NamespacedKey(this, "maxManaRate");

    public static Mage instance;
    public static void registerEvents(Listener listener) {
        instance.getServer().getPluginManager().registerEvents(listener, instance);
    }

    @Override
    public void onLoad() {
        if (this.getServer().getPluginManager().getPlugin("WorldGuard") != null)
            WorldGuardIntegration.registerWorldguardFlags();
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void onEnable() {
        Mage.instance = this;
        this.saveDefaultConfig();
        this.getLogger().warning("This is a development build. It is not intended for distribution and may contain bugs or unfinished features.");

        this.commandBind = new CommandBind(this);
        this.getCommand("bind").setExecutor(this.commandBind);
        this.getCommand("infuse").setExecutor(new CommandInfuse(this));
        this.getCommand("cast").setExecutor(new CommandCast(this));
        this.getCommand("setmana").setExecutor(new CommandSetMana(this));

        Mage.registerEvents(new CastListener(this));
        Mage.registerEvents(magiManager);

        for (Player player: this.getServer().getOnlinePlayers())
            player.sendMessage(ChatColor.GREEN + "Mage Ready");

        double rate = this.getConfig().getDouble("player.regen", 1);
        this.tick = new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<String, MagiPlayer> pair: magiManager.entrySet()) {
                    MagiPlayer magi = pair.getValue();
                    magi.setMana(magi.getMana() + rate);
                    magi.updateItemManaBar(magi.getPlayer().getInventory().getItemInMainHand());
                }
            }
        };
        this.tick.runTaskTimer(this, 0, 1);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        for (Map.Entry<String, MagiPlayer> pair: magiManager.entrySet())
            pair.getValue().updateBar(false);
        this.tick.cancel();
    }
}
