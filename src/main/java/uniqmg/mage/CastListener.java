package uniqmg.mage;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import org.bukkit.event.entity.EntityToggleSwimEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import uniqmg.mage.caster.MagiPlayer;
import uniqmg.mage.caster.SpellbookCaster;
import uniqmg.mage.caster.ToolCaster;
import uniqmg.mage.spellcraft.Spell;
import uniqmg.mage.spellcraft.SpellParser;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.syntax.FixedValue;
import uniqmg.mage.util.ToolType;

public class CastListener implements Listener {
    private Mage plugin;
    public CastListener(Mage plugin) {
        this.plugin = plugin;
    }

    // Pickaxe, shovel & axe spells trigger when breaking blocks
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent evt) {
        Player player = evt.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();

        ToolType.ToolCategory category = ToolType.toolCategoryOf(item.getType());
        if (
            category != ToolType.ToolCategory.PICKAXE &&
            category != ToolType.ToolCategory.SHOVEL &&
            category != ToolType.ToolCategory.AXE
        ) return;

        this.itemCast(player, item, new FixedValue<>(evt.getBlock().getLocation()));
    }

    // Sword & axe spells trigger when attacking something
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerHit(EntityDamageByEntityEvent evt) {
        if (!(evt.getDamager() instanceof Player)) return;
        Player player = (Player) evt.getDamager();
        ItemStack item = player.getInventory().getItemInMainHand();

        ToolType.ToolCategory category = ToolType.toolCategoryOf(item.getType());
        if (
            category != ToolType.ToolCategory.SWORD &&
            category != ToolType.ToolCategory.AXE
        ) return;

        this.itemCast(player, item, new FixedValue<>(evt.getEntity()));
    }

    // Elytra spells trigger when the player starts flying
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onGlideEvent(EntityToggleGlideEvent evt) {
        if (!(evt.getEntity() instanceof Player)) return;
        if (!evt.isGliding()) return;

        Player player = (Player) evt.getEntity();
        ItemStack elytra = player.getInventory().getItem(EquipmentSlot.CHEST);
        if (elytra == null || elytra.getType() != Material.ELYTRA) return;

        this.itemCast(player, elytra);
    }

    // Helmet spells trigger on the player starting to swim
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerSwim(EntityToggleSwimEvent evt) {
        if (!(evt.getEntity() instanceof Player) || !evt.isSwimming()) return;
        Player player = (Player) evt.getEntity();
        ItemStack helmet = player.getInventory().getItem(EquipmentSlot.HEAD);
        if (helmet == null || helmet.getType() == Material.ELYTRA) return;
        this.itemCast(player, helmet);
    }

    // Chestplate spells trigger on the player getting hurt
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerHurt(EntityDamageByEntityEvent evt) {
        if (!(evt.getEntity() instanceof Player)) return;
        Player player = (Player) evt.getEntity();
        ItemStack chestplate = player.getInventory().getItem(EquipmentSlot.CHEST);
        if (chestplate == null || chestplate.getType() == Material.ELYTRA) return;
        this.itemCast(player, chestplate, new FixedValue<>(evt.getDamager()), new FixedValue<>(evt.getDamage()));
    }

    // Leggings spells trigger on the player sneaking
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerSneak(PlayerToggleSneakEvent evt) {
        if (!evt.isSneaking()) return;
        Player player = evt.getPlayer();
        ItemStack leggings = player.getInventory().getItem(EquipmentSlot.LEGS);
        if (leggings == null) return;
        this.itemCast(player, leggings);
    }

    // Boots spells trigger on the player jumping
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerJump(PlayerJumpEvent evt) {
        Player player = evt.getPlayer();
        ItemStack boots = player.getInventory().getItem(EquipmentSlot.FEET);
        if (boots == null) return;
        this.itemCast(player, boots);
    }

    private void itemCast(Player player, ItemStack item, SpellComponent... components) {
        BookMeta boundSpell = plugin.commandBind.getBoundSpell(item);
        if (boundSpell == null) return;

        MagiPlayer magi = plugin.magiManager.magiOf(player);
        Spell spell = new SpellParser(magi).parse(boundSpell);
        if (spell == null) return;

        spell.cast(plugin, new ToolCaster(magi, item), components);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPrePlayerInteract(PlayerInteractEvent evt) {
        ItemStack hand = evt.getPlayer().getInventory().getItemInMainHand();
        // uncancels the event of right-clicking in the air with a book so you can itemCast spells
        // while not looking at a block
        if (evt.getAction() == Action.RIGHT_CLICK_AIR && evt.isCancelled() && SpellParser.isSpellbook(hand))
            evt.setCancelled(false);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent evt) {
        if (!(evt.getAction() == Action.RIGHT_CLICK_AIR || evt.getAction() == Action.RIGHT_CLICK_BLOCK)) return;

        Player player = evt.getPlayer();
        if (player.isSneaking()) return;

        MagiPlayer magi = plugin.magiManager.magiOf(player);
        ItemStack item = player.getInventory().getItemInMainHand();
        Spell spell = new SpellParser(magi).parse(item);

        if (spell != null) {
            spell.cast(plugin, new SpellbookCaster(magi, item));
            evt.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerHeldItemChange(PlayerItemHeldEvent evt) {
        MagiPlayer magi = plugin.magiManager.magiOf(evt.getPlayer());
        ItemStack item = evt.getPlayer().getInventory().getItem(evt.getNewSlot());
        magi.updateItemManaBar(item);
    }
}
