package uniqmg.mage.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.SpellParser;

import java.util.*;

public class CommandBind implements CommandExecutor {
    private Map<String, ItemStack> boundSpell = new HashMap<>();
    private final Mage plugin;

    private NamespacedKey bookTitle;
    private NamespacedKey bookAuthor;
    private NamespacedKey bookPageCount;

    public CommandBind(Mage plugin) {
        this.plugin = plugin;
        bookTitle = new NamespacedKey(this.plugin, "spellboundItemBookTitle");
        bookAuthor = new NamespacedKey(this.plugin, "spellboundItemBookName");
        bookPageCount = new NamespacedKey(this.plugin, "spellboundItemBookPageCount");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;
        String id = player.getUniqueId().toString();
        ItemStack hand = player.getInventory().getItemInMainHand();


        BookMeta existingMeta = this.getBoundSpell(hand);
        if (existingMeta != null) {
            ItemStack writtenBook = new ItemStack(Material.WRITTEN_BOOK);
            writtenBook.setItemMeta(existingMeta);
            player.getInventory().addItem(writtenBook);
            this.bindItem(hand, null);
            player.sendMessage(ChatColor.YELLOW + "Spellbook unbound");
            return true;
        }

        if (!boundSpell.containsKey(id)) {
            if (!SpellParser.isSpellbook(hand)) {
                player.sendMessage(ChatColor.RED + "Please hold a spellbook to bind");
                return true;
            }
            boundSpell.put(id, hand);
            player.getInventory().remove(hand);
            player.sendMessage(ChatColor.GOLD + "Hand stored");
        } else {
            ItemStack item = boundSpell.remove(id);
            if (hand.getType() == Material.AIR) {
                player.sendMessage(ChatColor.YELLOW + "Spellbook returned");
                player.getInventory().addItem(item);
                return true;
            }

            BookMeta meta = (BookMeta) item.getItemMeta();
            this.bindItem(hand, meta);

            player.sendMessage(ChatColor.GOLD + "Spellbook bound");
        }
        return true;
    }

    public void bindItem(ItemStack item, BookMeta meta) {
        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer ctag = itemMeta.getCustomTagContainer();

        if (meta == null) {
            if (ctag.hasCustomTag(bookPageCount, ItemTagType.INTEGER)) {
                @SuppressWarnings("ConstantConditions")
                int pageCount = ctag.getCustomTag(bookPageCount, ItemTagType.INTEGER);
                ctag.removeCustomTag(bookPageCount);
                ctag.removeCustomTag(bookAuthor);
                ctag.removeCustomTag(bookTitle);
                for (int i = 1; i <= pageCount; i++) {
                    NamespacedKey pagekey = new NamespacedKey(this.plugin, "spellboundItemPage" + i);
                    ctag.removeCustomTag(pagekey);
                }
            }
        } else {
            ctag.setCustomTag(bookPageCount, ItemTagType.INTEGER, meta.getPageCount());
            for (int i = 1; i <= meta.getPageCount(); i++) {
                NamespacedKey pagekey = new NamespacedKey(this.plugin, "spellboundItemPage" + i);
                ctag.setCustomTag(pagekey, ItemTagType.STRING, meta.getPage(i));
            }
            ctag.setCustomTag(bookAuthor, ItemTagType.STRING, Objects.requireNonNull(meta.getAuthor()));
            ctag.setCustomTag(bookTitle, ItemTagType.STRING, Objects.requireNonNull(meta.getTitle()));
        }

        item.setItemMeta(itemMeta);
    }

    public BookMeta getBoundSpell(ItemStack item) {
        ItemMeta itemMeta = item.getItemMeta();
        if (itemMeta == null) return null;
        CustomItemTagContainer ctag = itemMeta.getCustomTagContainer();

        if (!ctag.hasCustomTag(bookPageCount, ItemTagType.INTEGER)) return null;

        ItemStack writtenBook = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) writtenBook.getItemMeta();

        bookMeta.setTitle(ctag.getCustomTag(bookTitle, ItemTagType.STRING));
        bookMeta.setAuthor(ctag.getCustomTag(bookAuthor, ItemTagType.STRING));

        @SuppressWarnings("ConstantConditions")
        int pageCount = ctag.getCustomTag(bookPageCount, ItemTagType.INTEGER);
        ArrayList<String> pages = new ArrayList<>();
        pages.ensureCapacity(pageCount);
        for (int i = 1; i <= pageCount; i++) {
            NamespacedKey pagekey = new NamespacedKey(this.plugin, "spellboundItemPage" + i);
            pages.add(ctag.getCustomTag(pagekey, ItemTagType.STRING));
        }
        bookMeta.setPages(pages);

        return bookMeta;
    }
}
