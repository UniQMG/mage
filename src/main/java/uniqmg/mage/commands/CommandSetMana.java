package uniqmg.mage.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uniqmg.mage.Mage;

public class CommandSetMana implements CommandExecutor {
    private Mage plugin;
    public CommandSetMana(Mage plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player player = (Player) sender;

        try {
            this.plugin.magiManager.magiOf(player).setMaxMana(Integer.parseInt(args[0]));
            this.plugin.magiManager.magiOf(player).setMana(Integer.parseInt(args[0]));
            return true;
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }
}
