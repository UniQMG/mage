package uniqmg.mage.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import uniqmg.mage.Mage;
import uniqmg.mage.util.ToolType;

public class CommandInfuse implements CommandExecutor {
    private Mage plugin;

    public CommandInfuse(Mage plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;

        ItemStack item = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer ctag = itemMeta.getCustomTagContainer();

        Double mana = 0D;
        Double rate = 0D;
        switch (ToolType.classOf(item.getType())) {
            case DIAMOND:
                mana = plugin.getConfig().getDouble("tools.diamond.mana", 2000);
                rate = plugin.getConfig().getDouble("tools.diamond.rate", 2000);
                ctag.setCustomTag(plugin.mana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxMana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxManaRate, ItemTagType.DOUBLE, rate);
                break;
            case GOLD:
                mana = plugin.getConfig().getDouble("tools.gold.mana", 15000);
                rate = plugin.getConfig().getDouble("tools.gold.rate", 100);
                ctag.setCustomTag(plugin.mana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxMana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxManaRate, ItemTagType.DOUBLE, rate);
                break;
            case IRON:
                mana = plugin.getConfig().getDouble("tools.iron.mana", 1000);
                rate = plugin.getConfig().getDouble("tools.iron.rate", 10);
                ctag.setCustomTag(plugin.mana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxMana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxManaRate, ItemTagType.DOUBLE, rate);
                break;
            case WOOD:
                mana = plugin.getConfig().getDouble("tools.wood.mana", 250);
                rate = plugin.getConfig().getDouble("tools.wood.rate", 5);
                ctag.setCustomTag(plugin.mana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxMana, ItemTagType.DOUBLE, mana);
                ctag.setCustomTag(plugin.maxManaRate, ItemTagType.DOUBLE, rate);
                break;
            case OTHER:
                if (item.getType() == Material.ELYTRA) {
                    mana = plugin.getConfig().getDouble("tools.elytra.mana", 1500);
                    rate = plugin.getConfig().getDouble("tools.elytra.rate", 20);
                    ctag.setCustomTag(plugin.mana, ItemTagType.DOUBLE, mana);
                    ctag.setCustomTag(plugin.maxMana, ItemTagType.DOUBLE, mana);
                    ctag.setCustomTag(plugin.maxManaRate, ItemTagType.DOUBLE, rate);
                    break;
                }
            case NONE:
                player.sendMessage(ChatColor.RED + "Can't infuse that");
                return true;
        }

        player.sendMessage(String.format(
            "%sTool infused %s%s[%s%.0f%s / %s%.0f%s @ %s%.0f%s/t%s]",
            ChatColor.GOLD,
            ChatColor.GRAY,
            ChatColor.BOLD,
            ChatColor.BLUE,
            mana,
            ChatColor.GRAY,
            ChatColor.BLUE,
            mana,
            ChatColor.GRAY,
            ChatColor.BLUE,
            rate,
            ChatColor.GRAY,
            ChatColor.BOLD
        ));
        item.setItemMeta(itemMeta);
        return true;
    }
}
