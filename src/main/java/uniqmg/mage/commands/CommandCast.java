package uniqmg.mage.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uniqmg.mage.Mage;
import uniqmg.mage.caster.MagiPlayer;
import uniqmg.mage.spellcraft.Spell;
import uniqmg.mage.spellcraft.SpellParser;

public class CommandCast implements CommandExecutor {
    private Mage plugin;
    public CommandCast(Mage plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        sender.sendMessage(String.format(
            "%sCast%s>%s %s",
            ChatColor.BOLD,
            ChatColor.GRAY,
            ChatColor.RESET,
            String.join(" ", args)
        ));

        try {
            StringBuilder spellText = new StringBuilder();
            spellText.append("spell");
            for (String s: args) {
                spellText.append(" ");
                spellText.append(s);
            }

            Player player = (Player) sender;
            MagiPlayer magi = this.plugin.magiManager.magiOf(player);
            Spell spell = new SpellParser(magi).parse(spellText);
            if (spell != null) spell.cast(plugin, magi);
        } catch(Exception ex) {
            sender.sendMessage(ChatColor.RED + "Spell machine broke. An error has been logged to console.");
            ex.printStackTrace();
        }
        return true;
    }
}
