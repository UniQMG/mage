package uniqmg.mage;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import uniqmg.mage.caster.MagiPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MagiManager implements Listener {
    private HashMap<String, MagiPlayer> magis = new HashMap<>();

    private void setupPlayer(Player player) {
        MagiPlayer magi = new MagiPlayer(player);
        magi.updateBar(true);
        this.magis.put(player.getUniqueId().toString(), magi);
    }

    public Set<Map.Entry<String, MagiPlayer>> entrySet() {
        return magis.entrySet();
    }

    public MagiPlayer magiOf(Player player) {
        String key = player.getUniqueId().toString();
        if (!this.magis.containsKey(key))
            setupPlayer(player);
        return this.magis.get(key);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent evt) {
        evt.getPlayer().sendMessage("Welcome to the server, " + evt.getPlayer().getName());
        setupPlayer(evt.getPlayer());
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent evt) {
        this.magis.remove(evt.getPlayer().getUniqueId().toString());
    }
}