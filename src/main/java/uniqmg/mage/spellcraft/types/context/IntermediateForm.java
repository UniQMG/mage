package uniqmg.mage.spellcraft.types.context;

/**
 * Represents an intermediate representation of an object that can be
 * transformed back to it's original form including modifications.
 */
public interface IntermediateForm {
    /**
     * Gets the original form of the object wrapper
     * @return the original form
     */
    Object getOriginalForm();
}
