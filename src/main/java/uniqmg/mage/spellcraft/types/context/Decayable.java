package uniqmg.mage.spellcraft.types.context;

/**
 * Represents a value that can be distorted over time
 */
public interface Decayable {
    /**
     * Decays the value
     * @param ticks the number of ticks to decay it by (1 tick = 1/20th second)
     */
    void decay(int ticks);
}
