package uniqmg.mage.spellcraft.types.context;

/**
 * Represents an unstable object existing on a spell stack. Unstable objects
 * contribute instability and can cause a spell to experience Epic Failure
 */
public interface Unstable {
    double getInstability();
}
