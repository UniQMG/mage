package uniqmg.mage.spellcraft.types;

import org.bukkit.Location;
import org.bukkit.util.Vector;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;
import uniqmg.mage.spellcraft.types.context.Decayable;
import uniqmg.mage.spellcraft.types.context.IntermediateForm;

import javax.annotation.concurrent.Immutable;

/**
 * A wrapper class for representing a Location, a Vector, or three numbers as
 * an immutable, polymorphic construct capable of returning in it's source form.
 */
@Immutable
public class TripleTuple implements IntermediateForm, Decayable {
    private Vector vec;

    private enum TupleSource {
        Location,
        Vector,
        DoubleArray
    }
    private TupleSource source;
    private Location originalLocation;

    public TripleTuple(double x, double y, double z) {
        this.vec = new Vector(x, y, z);
        this.source = TupleSource.DoubleArray;
    }

    public TripleTuple(Vector vec) {
        this.vec = vec.clone();
        this.source = TupleSource.Vector;
    }

    public TripleTuple(Location loc) {
        this.vec = loc.toVector();
        this.originalLocation = loc.clone();
        this.source = TupleSource.Location;
    }

    private TripleTuple withCopiedSource(TripleTuple original) {
        this.source = original.source;
        if (this.source == TupleSource.Location)
            this.originalLocation = original.originalLocation.clone();
        return this;
    }

    public Location asLocation() {
        if (this.source != TupleSource.Location)
            throw new SpellRuntimeException("Expected a location vector, but got a " + this.source + " one.");
        return originalLocation.clone().set(vec.getX(), vec.getY(), vec.getZ());
    }

    @Override
    public void decay(int ticks) {
        double drift = 0.01 * ticks;
        vec.setX(vec.getX() + (Math.random()*2-1) * drift);
        vec.setY(vec.getY() + (Math.random()*2-1) * drift);
        vec.setZ(vec.getZ() + (Math.random()*2-1) * drift);
    }

    /**
     * Obtains the original form of the TripleTuple
     * @return a Location, Vector, or DoubleArray[3] array
     */
    @Override
    public Object getOriginalForm() {
        switch (source) {
            case Location:
                return originalLocation.clone().set(vec.getX(), vec.getY(), vec.getZ());

            case Vector:
                return this.vec.clone();

            case DoubleArray:
                return this;
        }

        assert false: "wtf";
        return null;
    }

    public double getX() { return vec.getX(); }
    public double getY() { return vec.getY(); }
    public double getZ() { return vec.getZ(); }

    /**
     * Adds together two TripleTuples
     * @param tuple that tuple
     * @return this tuple plus that tuple
     */
    public TripleTuple add(TripleTuple tuple) {
        return new TripleTuple(this.vec.clone().add(tuple.vec)).withCopiedSource(this);
    }

    /**
     * Subtracts the passed tuple from this tuple
     * @param tuple that tuple
     * @return this tuple minus that tuple
     */
    public TripleTuple subtract(TripleTuple tuple) {
        return new TripleTuple(this.vec.clone().subtract(tuple.vec)).withCopiedSource(this);
    }

    /**
     * Subtracts this tuple from the passed tuple
     * @param tuple that tuple
     * @return that tuple minus this tuple
     */
    public TripleTuple invertedSubtract(TripleTuple tuple) {
        return new TripleTuple(tuple.vec.clone().subtract(this.vec)).withCopiedSource(this);
    }

    /**
     * Multiplies two tuples together
     * @param tuple that tuple
     * @return this tuple times that tuple
     */
    public TripleTuple multiply(TripleTuple tuple) {
        return new TripleTuple(this.vec.clone().multiply(tuple.vec)).withCopiedSource(this);
    }

    /**
     * Multiplies a tuple by a scalar
     * @param scalar that scalar
     * @return this times that scalar
     */
    public TripleTuple multiply(Double scalar) {
        return new TripleTuple(this.vec.clone().multiply(scalar)).withCopiedSource(this);
    }

    /**
     * Divides this tuple by the passed tuple
     * @param tuple that tuple
     * @return this tuple divided by that tuple
     */
    public TripleTuple divide(TripleTuple tuple) {
        return new TripleTuple(this.vec.clone().divide(tuple.vec)).withCopiedSource(this);
    }

    /**
     * Divides the passed tuple by this tuple
     * @param tuple that tuple
     * @return that tuple divided by this tuple
     */
    public TripleTuple invertedDivide(TripleTuple tuple) {
        return new TripleTuple(tuple.vec.clone().divide(this.vec)).withCopiedSource(this);
    }

    public TripleTuple rotate(double yaw, double pitch) {
        return new TripleTuple(this.vec.clone().rotateAroundX(yaw).rotateAroundY(pitch)).withCopiedSource(this);
    }

    public TripleTuple normalize() {
        return new TripleTuple(this.vec.normalize()).withCopiedSource(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Location) obj = new TripleTuple((Location) obj);
        if (obj instanceof Vector) obj = new TripleTuple((Vector) obj);
        if (!(obj instanceof TripleTuple)) return false;
        TripleTuple tuple = (TripleTuple) obj;
        return tuple.vec.equals(this.vec);
    }

    @Override
    public String toString() {
        return String.format(
            "TripleTuple [%s, %s, %s] (of: %s)",
            this.vec.getX(),
            this.vec.getY(),
            this.vec.getZ(),
            this.source.toString()
        );
    }
}
