package uniqmg.mage.spellcraft.types;

import uniqmg.mage.spellcraft.types.context.Decayable;
import uniqmg.mage.spellcraft.types.context.Unstable;

import java.util.ArrayList;
import java.util.List;

public class StoredMana implements Decayable, Unstable {
    private double amount;
    private List<Runnable> exhaustedCallbacks = new ArrayList<>();

    public StoredMana(double amount) {
        this.setAmount(amount);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        if (!Double.isFinite(amount)) return;
        if (amount < 0) throw new IllegalArgumentException("Can't have negative mana");
        if (amount == 0 && this.amount != 0)
            for (Runnable task : exhaustedCallbacks)
                task.run();
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("Stored Mana (%.0f)", this.amount);
    }

    /**
     * Accepts a callback which is called whenever the stored mana's value is
     * set to 0 and was not previously 0. The callback can be called multiple
     * times but will not be called if the amount was already 0.
     * @param task the callback to be called
     */
    public void onExhausted(Runnable task) {
        this.exhaustedCallbacks.add(task);
    }

    @Override
    public void decay(int ticks) {
        double minLoss = 0.2;
        double lossRate = 0.1 * ticks * Math.log10(this.amount);
        double lossAmount = Math.random() * (1 - minLoss) + minLoss;
        double loss = lossRate * lossAmount;
        this.setAmount(Math.max(this.amount - loss, 0));
    }

    @Override
    public double getInstability() {
        return this.getAmount();
    }
}
