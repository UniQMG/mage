package uniqmg.mage.spellcraft;

import org.bukkit.Location;
import org.bukkit.util.Vector;
import uniqmg.mage.spellcraft.exception.SpellParameterException;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;
import uniqmg.mage.spellcraft.exception.SpellStackUnderflow;
import uniqmg.mage.spellcraft.types.TripleTuple;
import uniqmg.mage.spellcraft.types.context.Unstable;

import java.util.*;

public class SpellStack {
    /**
     * The stack is a stack of arbitrary values read and written from commands in
     * the script.
     */
    public final Stack<Object> stack = new Stack<>();

    @Override
    public String toString() {
        return "SpellStack (" + this.stack.size() + " elements)";
    }

    public double instability() {
        return this.instability(this, new Stack<>());
    }

    private double instability(SpellStack substack, Stack<SpellStack> history) {
        double instability = 0;
        for (Object obj: substack.stack) {
            if (obj instanceof Unstable) {
                instability += ((Unstable) obj).getInstability();
                continue;
            }

            if (obj instanceof SpellStack) {
                if (history.indexOf(obj) != -1) {
                    instability += 100;
                    continue;
                }
                history.push((SpellStack) obj);
                instability += this.instability((SpellStack) obj, history);
                history.pop();
            }
        }
        return instability;
    }

    /**
     * Pushes an object onto the stack. Numbers will be coerced to double.
     *
     * @param obj the object to push onto the stack
     */
    public void pushStack(Object obj) {
        Objects.requireNonNull(obj);
        if (obj instanceof Number)
            obj = ((Number) obj).doubleValue();
        if (this.stack.size() > 1000)
            throw new SpellRuntimeException("Stack size limit (1000) exceeded");
        this.stack.push(obj);
    }

    /**
     * See {@link #popSomeUnique(boolean, Class...)}
     * @param classes the types of objects to pop
     * @return the popped objects
     */
    public Map<Class, Object> popSomeUnique(Class... classes) {
        return this.popSomeUnique(false, classes);
    }

    /**
     * Returns objects from {@link #popSome(Class...)} inserted into a mapping
     * of Class->Object. Duplicate classes are not supported.
     * @param classes the requested types
     * @return a map of the type to the requested object
     */
    public Map<Class, Object> popSomeUnique(boolean required, Class... classes) {
        Map<Class, Object> objects = new HashMap<>();
        List<Class> classesLeft = new ArrayList<>(Arrays.asList(classes));

        int safety = 100;
        while (classesLeft.size() > 0 && (safety--) > 0) {
            try {
                RetrievedObjectContainer ret = this.popStackObjectContainer(false, classesLeft.toArray(new Class[0]));
                classesLeft.remove(ret.clazz);

                if (objects.containsKey(ret.clazz))
                    throw new IllegalArgumentException("Duplicate classes not permitted");
                objects.put(ret.clazz, ret.object);
            } catch(SpellRuntimeException ex) {
                if (required) throw ex;
                return objects;
            }
        }

        return objects;
    }

    /**
     * Pops multiple objects from the stack in any order. Each class provided will yield
     * one or no objects. Providing the same class multiple times will result in multiple
     * objects being provided if possible.
     * If no objects are retrieved from the stack, an empty list is returned.
     * Follows the same coercion rules as {@link #popStack(Class...)}
     * @param classes the requested types
     * @return objects of the requested types
     */
    public List<Object> popSome(Class... classes) {
        List<Object> objects = new ArrayList<>();
        List<Class> classesLeft = new ArrayList<>(Arrays.asList(classes));

        while (classesLeft.size() > 0) {
            try {
                RetrievedObjectContainer ret = this.popStackObjectContainer(false, classesLeft.toArray(new Class[0]));
                classesLeft.remove(ret.clazz);
                objects.add(ret.object);
            } catch(SpellRuntimeException ex) {
                break;
            }
        }

        return objects;
    }

    private class RetrievedObjectContainer {
        final Object object;
        final Class clazz;
        RetrievedObjectContainer(Object object, Class clazz) {
            this.object = object;
            this.clazz = clazz;
        }
    }

    /**
     * Pops an object, of one of multiple given types, from the stack
     * When given a number class, the object will attempt to be casted to it without java's boxing weirdness.
     * When given TripleTuple, a Location, Vector, or three Numbers will be coerced to it.
     * If null is given as a type, any object will be returned. If the stack underflows, null will be returned.
     *
     * @param classes multiple class references, one of which must match the top object on the stack
     * @return the popped object
     * @throws SpellParameterException if the stack underflows or the top object is not of the requested type
     */
    public Object popStack(Class... classes) {
        return this.popStackObjectContainer(false, classes).object;
    }

    /**
     * Peeks at an object, of one of multiple given types, on the stack.
     * See {@link #popStack(Class...)}.
     *
     * @param classes multiple class references, one of which must match the top object on the stack
     * @return the popped object
     * @throws SpellParameterException if the stack underflows or the top object is not of the requested type
     */
    public Object peekStack(Class... classes) {
        return this.popStackObjectContainer(true, classes).object;
    }

    /**
     * see {@link #popStack(Class...)}
     * @param classes the possible types of objects to pop, or null to pop any available object and ignore errors
     * @return an object of type of one of the given classes, or null if null was passed
     * @throws SpellParameterException if none of the given classes were found and null was not passed
     * @throws SpellStackUnderflow if the spell stack underflows while popping and null was not passed
     */
    private RetrievedObjectContainer popStackObjectContainer(boolean peekOnly, Class... classes) {
        if (this.stack.size() > 0) {
            Object obj = peekOnly ? this.stack.peek() : this.stack.pop();
            for (Class clazz: classes) {
                if (clazz == null) return new RetrievedObjectContainer(obj, null);

                if (clazz == TripleTuple.class) {
                    if (obj instanceof TripleTuple) return new RetrievedObjectContainer(obj, clazz);
                    if (obj instanceof Location) return new RetrievedObjectContainer(new TripleTuple((Location) obj), clazz);
                    if (obj instanceof Vector) return new RetrievedObjectContainer(new TripleTuple((Vector) obj), clazz);
                    if (
                            obj instanceof Number &&
                            stack.size() >= 2 &&
                            stack.get(stack.size() - 1) instanceof Number &&
                            stack.get(stack.size() - 2) instanceof Number
                    ) {
                        double z = (double) obj;
                        double y = ((Number) (peekOnly ? this.stack.get(stack.size() - 1) : this.stack.pop())).doubleValue();
                        double x = ((Number) (peekOnly ? this.stack.get(stack.size() - 2) : this.stack.pop())).doubleValue();
                        return new RetrievedObjectContainer(new TripleTuple(x, y, z), clazz);
                    }
                }

                if (obj instanceof Number) {
                    if (clazz == Number.class || clazz == Double.class)
                        return new RetrievedObjectContainer(((Number) obj).doubleValue(), clazz);

                    if (clazz == Integer.class)
                        return new RetrievedObjectContainer(((Number) obj).intValue(), clazz);

                    if (clazz == Byte.class)
                        return new RetrievedObjectContainer(((Byte) obj).longValue(), clazz);

                    if (clazz == Float.class)
                        return new RetrievedObjectContainer(((Number) obj).floatValue(), clazz);

                    if (clazz == Long.class)
                        return new RetrievedObjectContainer(((Number) obj).longValue(), clazz);

                    if (clazz == Short.class)
                        return new RetrievedObjectContainer(((Number) obj).shortValue(), clazz);
                }

                if (clazz.isInstance(obj))
                    return new RetrievedObjectContainer(obj, clazz);
            }
            // Can't find anything, put it back
            if (!peekOnly)
                this.stack.push(obj);
        }


        StringBuilder error = new StringBuilder();
        error.append("Expected ");
        boolean multiple = false;
        for (Class clazz: classes) {
            if (clazz == null) return new RetrievedObjectContainer(null, null);
            if (multiple) error.append(" or ");
            error.append(clazz.getSimpleName());
            multiple = true;
        }
        if (this.stack.size() > 0) {
            error.append(", got ");
            Object got = this.stack.peek();
            error.append((got == null ? "Null" : got.getClass().getSimpleName()));
            throw new SpellParameterException(error.toString());
        } else {
            error.append(" but stack underflowed");
            throw new SpellStackUnderflow(error.toString());
        }
    }
}