package uniqmg.mage.spellcraft;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.mage.Mage;
import uniqmg.mage.caster.*;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.syntax.Label;
import uniqmg.mage.spellcraft.exception.SpellInternalException;
import uniqmg.mage.spellcraft.exception.SpellOutOfManaException;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;
import uniqmg.mage.spellcraft.global.GlobalVariableManager;
import uniqmg.mage.util.protection.Protect;

import java.util.*;

public class CastingSpell extends BukkitRunnable {
    private Spell spell;
    private SpellCaster caster;
    private List<SpellComponent> components;
    private List<Runnable> spellTerminationCallbacks = new ArrayList<>();
    /**
     * Global variables are named values read and written from commands in the script by name,
     * but are shared between all scripts and have a habit of randomly decaying.
     */
    public final static GlobalVariableManager globals = new GlobalVariableManager();
    static { globals.startDecayCheck(Mage.instance, 10); }
    /**
     * Variables are named values read and written from commands in the script by name.
     */
    public final HashMap<String, Object> variables = new HashMap<>();
    /**
     * The call stack is a stack of positions that Return will jump to.
     * The numbers on the stack reference the line that was jumped from, and
     * upon returning, the instruction pointer will point at the NEXT instruction
     */
    public final Stack<Integer> callstack = new Stack<>();
    private final SpellStack spellStack = new SpellStack();
    private final CasterTickProxy proxy;
    public final Protect protection;
    private BossBar spellChargeBar;
    private int instruction = 0;
    private int speed = 1;

    /**
     * Creates a new casting spell, which is a BukkitRunnable form of a spell that actually
     * carries out the instructions.
     * @param spell the original spell blueprint
     * @param caster The caster casting the spell
     * @param components the components for the spell
     */
    public CastingSpell(Spell spell, SpellCaster caster, List<SpellComponent> components) {
        this.spell = spell;
        this.caster = caster;
        this.components = components;
        this.proxy = new CasterTickProxy(caster);
        this.spellChargeBar = Mage.instance.getServer().createBossBar("Spell Charge", BarColor.WHITE, BarStyle.SOLID);
        this.spellChargeBar.addPlayer(this.getPlayer());
        this.protection = new Protect(Mage.instance, this.getPlayer());

        System.out.println(String.format(
            "Begin cast. Mana: %.0f / %.0f (%.0f/s). Caster type: %s",
            caster.getMana(),
            caster.getMaximumMana(),
            caster.getManaRate(),
            caster.getClass().getSimpleName()
        ));
    }

    /**
     * Adds a runnable task that is called once the spell finishes executing.
     * @param task a runnable that will be called once the spell terminates.
     */
    public void onTerminate(Runnable task) {
        spellTerminationCallbacks.add(task);
    }

    /**
     * Raises an interrupt, which causes the spell to call a specific subroutine
     * This works similarly to the call command, and pushes the current location to the callstack
     * Note that only the instruction pointer will be changed, the actual interrupt script won't
     * be run until the spell steps normally during the next game tick.
     * If the interrupt doesn't exist, nothing happens.
     * @param labelName the name of the interrupt label to jump to
     */
    public void interrupt(String labelName) {
        int location = this.getLabelLocation(labelName);
        if (location == -1) return;

        this.callstack.push(this.getInstruction()-1);
        this.setInstruction(location);
    }

    @Override
    public void cancel() {
        super.cancel();
        for (Runnable task: this.spellTerminationCallbacks)
            task.run();
        this.spellTerminationCallbacks.clear();
        this.spellChargeBar.removeAll();
    }

    @Override
    public void run() {
        proxy.tick();
        for (int i = 0; i < this.speed; i++) {
            if (this.isCancelled()) return;
            this.stepInstruction();
        }

        if (this.instruction < this.components.size()) {
            SpellComponent component = this.components.get(instruction);
            double cost = 0;
            try {
                cost = component.getCost(this);
            } catch(SpellRuntimeException ignored) {}
            double charge = this.proxy.getCharge();
            if (cost == 0) cost = 1;
            spellChargeBar.setVisible(cost > this.proxy.getManaRate());
            spellChargeBar.setProgress(1 - Math.min(charge / cost, 1));
            spellChargeBar.setTitle(String.format(
                "Spell: %s (#%s) [%.0f/s]",
                component.getClass().getSimpleName(),
                this.instruction,
                this.proxy.getManaRate()
            ));
        }
    }

    /**
     * Runs the next spell instruction
     */
    private void stepInstruction() {
        if (instruction >= this.components.size()) {
            this.cancel();
            return;
        }

        SpellComponent component = this.components.get(instruction);
        System.out.println("Process component " + component.getClass().getSimpleName());

        double instability = this.spellStack.instability();
        System.out.println("Stability: " + instability);
        if (instability >= 2500) {
            this.getPlayer().sendMessage("The spell blows up in your face");
            Location loc = this.getPlayer().getLocation();
            loc.getWorld().createExplosion(loc, 10, false, false);
            this.cancel();
        }

        try {
            double cost = component.getCost(this);

            if (!this.proxy.chargeTo(cost)) {
                if (this.proxy.getMana() == 0)
                    throw new SpellOutOfManaException("Spell out of mana");
                return;
            }

            if (!this.proxy.consumeMana(cost))
                throw new SpellOutOfManaException("Spell out of mana");

            SpellComponentResult result = component.apply(this);

            if (result == null) {
                this.instruction++;
                return;
            }

            switch (result) {
                case CONTINUE:
                    this.instruction++;
                    break;

                case REPEAT_INSTRUCTION:
                    break;

                case TERMINATE:
                    this.cancel();
                    break;
            }
        } catch(SpellRuntimeException ex) {
            Player caster = this.getPlayer();
            if (caster != null) caster.sendMessage(String.format(
                "%s%sSpell error: %s%s%s%s %sin component %s%s%s",
                ChatColor.RED,
                ChatColor.BOLD,

                ChatColor.RESET,
                ChatColor.UNDERLINE,
                ex.getMessage(),
                ChatColor.RESET,

                ChatColor.RED,

                ChatColor.RESET,
                ChatColor.GOLD,
                component.getClass().getSimpleName()
            ));
            ex.printStackTrace();
            this.cancel();
        } catch (Exception ex) {
            this.cancel();
            ex.printStackTrace();
            Player caster = this.getPlayer();
            if (caster != null) caster.sendMessage("Internal exception while running spell.");
        }
    }

    /**
     * Gets the current speed of the spell
     * @return the speed of the spell in instructions per game tick
     */
    public int getSpeed() {
        return this.speed;
    }

    /**
     * Sets the current speed of the spell
     * @param speed the new speed in instructions per game tick
     */
    public void setSpeed(int speed) {
        if (speed == 0) throw new SpellRuntimeException("Spell speed must be greater than 0");
        this.speed = speed;
    }

    /**
     * @return the current instruction pointer
     */
    public int getInstruction() {
        return this.instruction;
    }

    /**
     * Sets the instruction pointer
     * @param instruction what instruction to execute next
     */
    public void setInstruction(int instruction) {
        if (instruction < 0 || instruction > this.components.size())
            throw new SpellInternalException("Instruction out of bounds");
        this.instruction = instruction;
    }

    /**
     * Attempts to find the player responsible for the spell. Should work in most cases.
     * @return the player casting the spell, or null
     */
    public Player getPlayer() {
        try {
            return this.getMagiPlayer().getPlayer();
        } catch(SpellRuntimeException ex) {
            return null;
        }
    }

    /**
     * Attemps to find the MagiPlayer responsible for the spell. Should work in most cases.
     * @return the MagiPlayer responsible for the spell
     * @throws SpellRuntimeException if the magiplayer is unavailable from this context
     */
    public MagiPlayer getMagiPlayer() {
        if (this.caster instanceof SpellbookCaster) return ((SpellbookCaster) this.caster).player;
        if (this.caster instanceof ToolCaster) return ((ToolCaster) this.caster).player;
        if (this.caster instanceof MagiPlayer) return (MagiPlayer) this.caster;
        throw new SpellRuntimeException("Player not available from this context");
    }

    /**
     * Gets the SpellCaster casting the spell
     * @return the SpellCaster casting the spell
     */
    public SpellCaster getCaster() {
        return this.caster;
    }

    /**
     * Gets the spell components which are the instructions of the spell whose index
     * corresponds to the current spell instruction pointer
     * @return a list of the spell's components
     */
    public List<SpellComponent> getComponents() {
        return Collections.unmodifiableList(this.components);
    }

    /**
     * Gets the spellstack, which stores values generated by the spell
     * @return the spell's stack
     */
    public SpellStack getSpellStack() {
        return spellStack;
    }

    /**
     * Gets the instruction index of a label
     * @param labelName the name of the label
     * @return the first encountered index of the label, or -1 if not found
     */
    public int getLabelLocation(String labelName) {
        for (int i = 0; i < components.size(); i++) {
            SpellComponent component = components.get(i);
            if (component instanceof Label && ((Label) component).labelName.equals(labelName)) {
                return i;
            }
        }
        return -1;
    }
}
