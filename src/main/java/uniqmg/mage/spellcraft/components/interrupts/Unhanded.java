package uniqmg.mage.spellcraft.components.interrupts;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Unhanded implements SpellComponent, Listener {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Unhanded();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = spell.getMagiPlayer().getPlayer();
        ItemStack hand = player.getInventory().getItemInMainHand();

        String interruptName = (String) spell.getSpellStack().popStack(String.class);
        Listener listener = new Listener() {
            @EventHandler
            public void onHeldItemChange(PlayerItemHeldEvent evt) {
                if (!evt.getPlayer().equals(player)) return;
                spell.interrupt(interruptName);
            }
        };
        Mage.registerEvents(listener);
        spell.onTerminate(() -> PlayerItemHeldEvent.getHandlerList().unregister(listener));
        return null;
    }


}
