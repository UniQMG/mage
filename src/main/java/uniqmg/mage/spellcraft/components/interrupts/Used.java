package uniqmg.mage.spellcraft.components.interrupts;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import uniqmg.mage.Mage;
import uniqmg.mage.caster.ItemCaster;
import uniqmg.mage.caster.SpellCaster;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

import java.util.HashMap;
import java.util.UUID;

public class Used implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Used();
    }

    private static NamespacedKey usedId = new NamespacedKey(Mage.instance, "usedId");
    private static HashMap<String, UseListener> listeners = new HashMap<>();

    private static class UseListener implements Listener {
        private String id;
        private ItemStack item;
        private CastingSpell spell;
        private String interruptName;

        public UseListener(String id, ItemStack item, CastingSpell spell, String interruptName) {
            this.id = id;
            this.item = item;
            this.spell = spell;
            this.interruptName = interruptName;

            listeners.put(id, this);
            Mage.registerEvents(this);
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onBlockBreak(BlockBreakEvent evt) {
            ItemStack hand = evt.getPlayer().getInventory().getItemInMainHand();
            ItemMeta meta = hand.getItemMeta();
            if (meta == null) return;
            CustomItemTagContainer ctag = meta.getCustomTagContainer();
            String retrievedUse = ctag.getCustomTag(usedId, ItemTagType.STRING);

            if (this.id.equals(retrievedUse)) {
                spell.interrupt(interruptName);
                evt.setCancelled(true);
            }
        }

        @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
        public void onPlayerInteract(PlayerInteractEvent evt) {
            ItemStack hand = evt.getPlayer().getInventory().getItemInMainHand();
            ItemMeta meta = hand.getItemMeta();
            if (meta == null) return;
            CustomItemTagContainer ctag = meta.getCustomTagContainer();
            String retrievedUse = ctag.getCustomTag(usedId, ItemTagType.STRING);

            if (this.id.equals(retrievedUse)) {
                spell.interrupt(interruptName);
                evt.setCancelled(true);
            }
        }

        public void cancel() {
            PlayerItemHeldEvent.getHandlerList().unregister(this);
            BlockBreakEvent.getHandlerList().unregister(this);
            listeners.remove(id);

            ItemMeta meta = item.getItemMeta();
            meta.getCustomTagContainer().removeCustomTag(usedId);
            item.setItemMeta(meta);
        }
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        SpellCaster caster = spell.getCaster();
        if (!(caster instanceof ItemCaster))
            throw new SpellRuntimeException("No item to use when casting from this context");

        ItemStack item = ((ItemCaster) caster).getItem();
        if (item.getType() == Material.AIR)
            throw new SpellRuntimeException("No item to use");

        ItemMeta itemMeta = item.getItemMeta();
        CustomItemTagContainer ctag = itemMeta.getCustomTagContainer();

        if (ctag.hasCustomTag(usedId, ItemTagType.STRING)) {
            String id = ctag.getCustomTag(usedId, ItemTagType.STRING);
            UseListener listener = listeners.get(id);
            if (listener != null) listener.cancel();
        }

        String useIdentifier = UUID.randomUUID().toString();
        ctag.setCustomTag(usedId, ItemTagType.STRING, useIdentifier);
        item.setItemMeta(itemMeta);

        String interruptName = (String) spell.getSpellStack().popStack(String.class);
        UseListener listener = new UseListener(useIdentifier, item, spell, interruptName);

        spell.onTerminate(listener::cancel);
        return null;
    }
}
