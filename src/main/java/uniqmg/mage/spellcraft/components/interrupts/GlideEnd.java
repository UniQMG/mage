package uniqmg.mage.spellcraft.components.interrupts;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class GlideEnd implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new GlideEnd();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = spell.getMagiPlayer().getPlayer();
        String label = (String) spell.getSpellStack().popStack(String.class);

        Listener listener = new Listener() {
            @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
            public void onGlideEvent(EntityToggleGlideEvent evt) {
                if (!evt.getEntity().equals(player)) return;
                if (evt.isGliding()) return;
                spell.interrupt(label);
            }
        };

        Mage.registerEvents(listener);
        spell.onTerminate(() -> EntityToggleGlideEvent.getHandlerList().unregister(listener));
        return null;
    }
}
