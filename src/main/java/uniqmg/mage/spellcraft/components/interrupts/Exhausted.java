package uniqmg.mage.spellcraft.components.interrupts;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

import java.util.Map;

public class Exhausted implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Exhausted();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Map<Class, Object> objects = spell.getSpellStack().popSomeUnique(true, StoredMana.class, String.class);
        StoredMana mana = (StoredMana) objects.get(StoredMana.class);
        String label = (String) objects.get(String.class);
        mana.onExhausted(() -> spell.interrupt(label));
        return null;
    }
}
