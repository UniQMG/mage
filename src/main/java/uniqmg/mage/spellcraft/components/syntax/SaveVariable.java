package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class SaveVariable implements SpellComponent {
    private String variable;

    public SaveVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new SaveVariable(this.variable);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {

        String variable = this.variable;
        if (variable == null) variable = (String) spell.getSpellStack().popStack(String.class);

        spell.variables.put(variable, spell.getSpellStack().popStack(Object.class));
        return null;
    }
}
