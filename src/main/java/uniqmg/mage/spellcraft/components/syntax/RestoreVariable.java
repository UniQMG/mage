package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class RestoreVariable implements SpellComponent {
    private String variable;

    public RestoreVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new RestoreVariable(variable);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        String variable = this.variable;
        if (variable == null) variable = (String) spell.getSpellStack().popStack(String.class);
        Object obj = spell.variables.get(variable);
        if (obj == null) throw new SpellRuntimeException("Unknown variable " + variable);
        spell.getSpellStack().stack.push(obj);
        return null;
    }
}
