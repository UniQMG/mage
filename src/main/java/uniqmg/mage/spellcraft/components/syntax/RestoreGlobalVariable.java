package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class RestoreGlobalVariable implements SpellComponent {
    private String variable;

    public RestoreGlobalVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new RestoreGlobalVariable(variable);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        String variable = this.variable;
        if (variable == null) variable = (String) spell.getSpellStack().popStack(String.class);
        Object obj = CastingSpell.globals.get(spell.getPlayer(), variable);
        if (obj == null) throw new SpellRuntimeException("Unknown variable " + variable);
        spell.getSpellStack().stack.push(obj);
        return null;
    }
}