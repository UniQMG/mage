package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class FixedValue<T> implements SpellComponent {
    private T value;
    public FixedValue(T value) {
        this.value = value;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new FixedValue<>(this.value);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(this.value);
        return null;
    }
}
