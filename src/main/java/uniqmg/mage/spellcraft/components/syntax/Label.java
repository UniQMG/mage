package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Label implements SpellComponent {
    public final String labelName;
    public Label(String labelName) {
        this.labelName = labelName;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new Label(labelName);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        return null;
    }
}
