package uniqmg.mage.spellcraft.components.syntax;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class SaveGlobalVariable implements SpellComponent {
    private String variable;

    public SaveGlobalVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new SaveGlobalVariable(variable);
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        String variable = this.variable;
        if (variable == null) variable = (String) spell.getSpellStack().popStack(String.class);
        CastingSpell.globals.put(spell.getPlayer(), variable, spell.getSpellStack().popStack(Object.class));
        return null;
    }
}
