package uniqmg.mage.spellcraft.components.magic.elements;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.util.Util;
import uniqmg.mage.util.geometry.SphereBuilder;

public class Earth implements SpellComponent, Element {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Earth();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(this);
        return null;
    }

    @Override
    public void spawnParticles(Location location, double power) {
        double size = Math.min(Math.sqrt(power / 100) + 1, 2) * 0.25;
        int particles = (int) Math.min(Math.log(power/100 + 1) / Math.log(1.1), 100);
        BlockData data = Material.STONE.createBlockData();
        location.getWorld().spawnParticle(Particle.BLOCK_DUST, location, particles, size, size, size, data);
    }

    @Override
    public void applyPassive(CastingSpell spell, Location location, double power) {
        int radius = (int) Math.min(power/50 + 3, 25);

        for (int i = 0; i < Math.max(radius * 5, 1); i++) {
            Block block = Util.randomNearby(location, radius).getBlock();

            if (block.getType() == Material.COBBLESTONE && Math.random() > 0.75)
                spell.protection.setBlock(block, Material.GRAVEL);

            if (block.getType() == Material.STONE)
                spell.protection.setBlock(block, Material.COBBLESTONE);

            if (block.getType() == Material.DIRT)
                spell.protection.setBlock(block, Material.COARSE_DIRT);

            if (block.getType() == Material.GRASS_BLOCK)
                spell.protection.setBlock(block, Material.DIRT);
        }

        if (power < 200) return;
        int amplifier = (int) Math.min(power / 200, 5);
        Util.potionAoE(PotionEffectType.DAMAGE_RESISTANCE, location, 3, 100, amplifier, 0.5);
    }

    @Override
    public void applyActive(CastingSpell spell, Location location, double power) {
        double radius = SphereBuilder.radius(power);
        SphereBuilder sphere = new SphereBuilder(location, Math.min(radius, 10));

        double breakBias = Math.min(power / 500, 1) * 0.1;

        Location loc;
        while ((loc = sphere.next()) != null) {
            Block block = loc.getBlock();
            if (block.getType().getHardness() == -1) continue;
            BlockData blockData = block.getBlockData();

            double random = Math.random();

            if (random < (0.25 - breakBias)) {
                if (block.getType().isSolid() && spell.protection.hasPermission(block)) {
                    block.setType(Material.AIR);
                    FallingBlock ent = loc.getWorld().spawnFallingBlock(loc, blockData);
                    ent.setVelocity(new Vector(
                        (Math.random()*2-1) * Math.min(power / 500, 0.20),
                        Math.random() * Math.min(power / 500, 0.5) + 0.25,
                        (Math.random()*2-1) * Math.min(power / 500, 0.20)
                    ));
                }
            } else if (random < (0.5 - breakBias*2)) {
                spell.protection.breakBlock(block);
            } else {
                spell.protection.setBlock(block, Material.AIR);
            }
        }
    }

    @Override
    public void applyActive(CastingSpell spell, Entity entity, double power) {
        this.applyActive(spell, entity.getLocation(), power);
        if (entity instanceof LivingEntity)
            ((LivingEntity) entity).damage(power/10, spell.getPlayer());
    }

    @Override
    public void applySpecial(CastingSpell spell, Location location, double power) {
        Block block = location.getWorld().getBlockAt(location);
        float hardness = block.getType().getHardness();
        if (hardness == -1.0 || hardness > power) return;
        spell.protection.breakBlock(block);
    }
}
