package uniqmg.mage.spellcraft.components.magic.shapes;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.spellcraft.components.magic.elements.Pure;
import uniqmg.mage.spellcraft.types.StoredMana;

import java.util.Map;

public class ShapeBall implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new ShapeBall();
    }

    private class ShapeBallListener extends BukkitRunnable implements Listener {
        private CastingSpell spell;
        private final Fireball fireball;
        private Element element;
        private double power;

        public ShapeBallListener(CastingSpell spell, Fireball fireball, Element element, double power) {
            this.spell = spell;
            this.fireball = fireball;
            this.element = element;
            this.power = power;
        }

        public void start() {
            Mage.registerEvents(this);
            this.runTaskTimer(Mage.instance, 0, 1);
        }

        @EventHandler
        public void onFireballHit(ProjectileHitEvent evt) {
            Entity ent = evt.getEntity();
            if (!(ent instanceof Fireball)) return;

            Entity hitEntity = evt.getHitEntity();

            Fireball fireball = (Fireball) ent;
            if (!fireball.equals(this.fireball)) return;

            if (hitEntity != null) {
                element.applyActive(spell, hitEntity, this.power);
            } else {
                element.applyActive(spell, fireball.getLocation(), this.power);
            }

            ent.remove();
            this.cancel();
        }

        @EventHandler
        public void onFireballDamage(EntityDamageByEntityEvent evt) {
            Entity ent = evt.getDamager();
            if (!(ent instanceof Fireball)) return;

            Fireball fireball = (Fireball) ent;
            if (!fireball.equals(this.fireball)) return;

            evt.setCancelled(true);
        }

        @Override
        public void run() {
            element.spawnParticles(fireball.getLocation(), this.power);
            element.applyPassive(spell, fireball.getLocation(), this.power);
        }

        @Override
        public void cancel() {
            EntityDamageByEntityEvent.getHandlerList().unregister(this);
            ProjectileHitEvent.getHandlerList().unregister(this);
            super.cancel();
        }
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Map<Class, Object> args = spell.getSpellStack().popSomeUnique(StoredMana.class, Element.class);
        StoredMana spellCharge = (StoredMana) args.get(StoredMana.class);
        Element element = (Element) args.get(Element.class);

        if (element == null) element = new Pure();
        double charge = 10 + (spellCharge != null ? spellCharge.getAmount() : 0);
        if (spellCharge != null) spellCharge.setAmount(0);

        //System.out.println(String.format("Casting %s ball at power %s", element, charge));

        Player player = spell.getMagiPlayer().getPlayer();
        World world = player.getLocation().getWorld();

        Fireball fireball = world.spawn(player.getEyeLocation(), Fireball.class);
        fireball.setDirection(player.getLocation().getDirection());
        fireball.setIsIncendiary(false);
        fireball.setShooter(player);

        new ShapeBallListener(spell, fireball, element, charge).start();
        return null;
    }

}
