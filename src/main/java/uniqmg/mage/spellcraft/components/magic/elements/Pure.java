package uniqmg.mage.spellcraft.components.magic.elements;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.magic.Element;

import java.util.Collection;

public class Pure implements Element {
    @Override
    public void spawnParticles(Location location, double power) {
        double size = Math.min(Math.sqrt(power / 100) + 1, 2) * 0.25;
        int particles = (int) Math.min(power/5 + 1, 100);
        Particle.DustOptions dustOptions = new Particle.DustOptions(Color.WHITE, 1);
        location.getWorld().spawnParticle(Particle.REDSTONE, location, particles, size, size, size, dustOptions);
    }

    @Override
    public void applyPassive(CastingSpell spell, Location location, double power) {
    }

    @Override
    public void applyActive(CastingSpell spell, Location location, double power) {
        Collection<LivingEntity> entities = location.getWorld().getNearbyLivingEntities(location, 5);
        for (LivingEntity ent: entities)
            ent.damage(power / 50, spell.getPlayer());
    }

    @Override
    public void applyActive(CastingSpell spell, Entity entity, double power) {
        if (entity instanceof LivingEntity)
            ((LivingEntity) entity).damage(power / 25, spell.getPlayer());
    }

    @Override
    public void applySpecial(CastingSpell spell, Location location, double power) {
    }
}
