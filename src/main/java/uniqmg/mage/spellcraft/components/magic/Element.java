package uniqmg.mage.spellcraft.components.magic;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import uniqmg.mage.spellcraft.CastingSpell;

public interface Element {
    void spawnParticles(Location location, double power);
    void applyPassive(CastingSpell spell, Location location, double power);
    void applyActive(CastingSpell spell, Location location, double power);
    void applyActive(CastingSpell spell, Entity entity, double power);
    void applySpecial(CastingSpell spell, Location location, double power);

    // TODO: Not sure if these are going to be actually used
    //void applyPassive(CastingSpell spell, Entity entity, double power);
    //void applySpecial(CastingSpell spell, Entity entity, double power);
}
