package uniqmg.mage.spellcraft.components.magic.mana;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

public class Charge implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return (double) spell.getSpellStack().peekStack(Number.class);
    }

    @Override
    public SpellComponent createCopy() {
        return new Charge();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        double quantity = (double) spell.getSpellStack().popStack(Number.class);
        StoredMana mana = new StoredMana(quantity);
        spell.getSpellStack().pushStack(mana);
        return null;
    }
}
