package uniqmg.mage.spellcraft.components.magic.mana;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

import java.util.Map;

public class Recharge implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        Map<Class, Object> objs = spell.getSpellStack().popSomeUnique(true, Number.class, StoredMana.class);
        spell.getSpellStack().pushStack(objs.get(StoredMana.class));
        spell.getSpellStack().pushStack(objs.get(Number.class));
        return (double) objs.get(Number.class);
    }

    @Override
    public SpellComponent createCopy() {
        return new Recharge();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Map<Class, Object> objs = spell.getSpellStack().popSomeUnique(true, Number.class, StoredMana.class);
        StoredMana mana = (StoredMana) objs.get(StoredMana.class);
        double quantity = (double) objs.get(Number.class);
        mana.setAmount(mana.getAmount() + quantity);
        spell.getSpellStack().pushStack(mana);
        return null;
    }
}
