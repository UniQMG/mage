package uniqmg.mage.spellcraft.components.magic.elements;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Particle.DustOptions;
import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffectType;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.util.Util;
import uniqmg.mage.util.geometry.SphereBuilder;

public class Water implements SpellComponent, Element {
    @Override
    public void spawnParticles(Location location, double power) {
        double size = Math.min(Math.sqrt(power / 100) + 1, 2) * 0.25;
        int particles = (int) Math.min(power/5 + 1, 100);
        DustOptions dustOptions = new DustOptions(Color.fromRGB(0, 127, 255), 1);
        location.getWorld().spawnParticle(Particle.REDSTONE, location, particles, size, size, size, dustOptions);
    }

    @Override
    public void applyPassive(CastingSpell spell, Location location, double power) {
        if (power < 50) return;
        double radius = Math.min(power/20 + 3, 12);
        for (int i = 0; i < 25; i++) {
            Location loc = Util.randomNearby(location, radius);
            Block block = loc.getBlock();
            if (block.getType() == Material.FIRE)
                spell.protection.setBlock(block, Material.AIR);
        }

        if (power < 200) return;
        Util.potionAoE(PotionEffectType.WATER_BREATHING, location, 3);
    }

    @Override
    public void applyActive(CastingSpell spell, Location location, double power) {
        if (power < 100) {
            int newLevel = (int) Math.ceil(7 - (Math.min(power / 50, 1) * 7));
            this.spawnWater(spell, location, newLevel);
            return;
        }

        double radius = SphereBuilder.radius(power / 50);
        SphereBuilder sphere = new SphereBuilder(location, Math.min(radius, 10));

        Location next;
        while ((next = sphere.next()) != null)
            this.spawnWater(spell, next, 0);
    }

    @Override
    public void applyActive(CastingSpell spell, Entity entity, double power) {
        this.applyActive(spell, entity.getLocation(), power);
        if (!(entity instanceof LivingEntity)) return;
        LivingEntity live = (LivingEntity) entity;
        live.setRemainingAir((int) Math.max(live.getRemainingAir() - power, 0));
    }

    @Override
    public void applySpecial(CastingSpell spell, Location location, double power) {
        if (power < 10) return;
        this.spawnWater(spell, location, 0);
    }

    private void spawnWater(CastingSpell spell, Location location, int waterLevel) {
        Block block = location.getBlock();
        if (!block.isPassable()) return;
        if (!spell.protection.hasPermission(block)) return;
        if (block.getType() != Material.AIR) block.breakNaturally();
        block.setType(Material.WATER);

        if (block.getType() == Material.WATER) {
            Levelled level = (Levelled) block.getBlockData();

            // 0 is a source block, 7 is highest, 1 is lowest
            level.setLevel(waterLevel);
            block.setBlockData(level);
        }
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Water();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(this);
        return null;
    }
}
