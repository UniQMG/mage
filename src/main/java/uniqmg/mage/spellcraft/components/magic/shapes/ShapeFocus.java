package uniqmg.mage.spellcraft.components.magic.shapes;

import org.bukkit.Location;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.spellcraft.components.magic.elements.Pure;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;
import uniqmg.mage.spellcraft.types.StoredMana;
import uniqmg.mage.spellcraft.types.TripleTuple;

import java.util.Map;

public class ShapeFocus implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new ShapeFocus();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Map<Class, Object> args = spell.getSpellStack().popSomeUnique(
            StoredMana.class, Element.class, TripleTuple.class
        );

        TripleTuple tuple = (TripleTuple) args.get(TripleTuple.class);
        if (tuple == null) throw new SpellRuntimeException("Expected Location on stack");
        Location location = tuple.asLocation();
        StoredMana spellCharge = (StoredMana) args.get(StoredMana.class);
        Element element = (Element) args.get(Element.class);

        if (element == null) element = new Pure();
        double charge = 10 + (spellCharge != null ? spellCharge.getAmount() : 0);
        if (spellCharge != null) spellCharge.setAmount(0);

        element.spawnParticles(location, charge);
        element.applyActive(spell, location, charge);
        return null;
    }
}
