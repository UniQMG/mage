package uniqmg.mage.spellcraft.components.magic.elements;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.potion.PotionEffectType;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.util.Util;

public class Fire implements SpellComponent, Element {
    @Override
    public void spawnParticles(Location location, double power) {
        double size = Math.min(Math.sqrt(power / 100) + 1, 2) * 0.25;
        int particles = (int) Math.min(Math.log(power/100 + 1) / Math.log(1.1), 100);
        location.getWorld().spawnParticle(Particle.FLAME, location, particles, size, size, size, 0);
    }

    @Override
    public void applyPassive(CastingSpell spell, Location location, double power) {
        if (power < 50) return;
        int radius = (int) Math.min(power/50 + 3, 10);

        for (int i = 0; i < radius * 5; i++) {
            Block block = Util.randomNearby(location, radius).getBlock();

            if (block.getType() == Material.SNOW)
                spell.protection.breakBlock(block);

            if (power < 100) continue;
            if (block.getType().isBurnable() || block.getType() == Material.AIR)
                spell.protection.setBlock(block, Material.FIRE);

            if (block.getType() == Material.COBBLESTONE)
                spell.protection.setBlock(block, Material.STONE);

            if (block.getType() == Material.GRASS_BLOCK)
                spell.protection.setBlock(block, Material.DIRT);

            if (power < 500) continue;
            if (block.getType() == Material.WATER)
                spell.protection.setBlock(block, Material.AIR);

            if (block.getType() == Material.ICE || block.getType() == Material.PACKED_ICE)
                location.getWorld().createExplosion(block.getLocation(), 100);
        }

        if (power < 200) return;
        Util.potionAoE(PotionEffectType.FIRE_RESISTANCE, location, 3);
    }

    @Override
    public void applyActive(CastingSpell spell, Location location, double power) {
        double effectivePower = Math.min(Math.sqrt(power / 50), 100);
        location.getWorld().createExplosion(location, (float) effectivePower, power > 50, power > 100);
    }

    @Override
    public void applyActive(CastingSpell spell, Entity entity, double power) {
        this.applyActive(spell, entity.getLocation(), power);
    }

    @Override
    public void applySpecial(CastingSpell spell, Location location, double power) {
        if (power > 100) {
            spell.protection.setBlock(location.getBlock(), Material.LAVA);
            return;
        }

        if (power > 10) {
            spell.protection.setBlock(location.getBlock(), Material.LAVA);
        }
    }

    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Fire();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(this);
        return null;
    }
}
