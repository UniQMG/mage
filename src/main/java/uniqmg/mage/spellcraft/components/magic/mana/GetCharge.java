package uniqmg.mage.spellcraft.components.magic.mana;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

public class GetCharge implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new GetCharge();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        StoredMana mana = (StoredMana) spell.getSpellStack().popStack(StoredMana.class);
        spell.getSpellStack().pushStack(mana.getAmount());
        return null;
    }
}
