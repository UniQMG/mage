package uniqmg.mage.spellcraft.components.magic.shapes;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.spellcraft.components.magic.elements.Pure;
import uniqmg.mage.spellcraft.types.StoredMana;

import java.util.Map;

public class ShapeAura implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new ShapeAura();
    }

    private class ShapeAuraTicker extends BukkitRunnable {
        private CastingSpell spell;
        private final Player player;
        private Element element;
        private StoredMana charge;
        private double tick = 0;

        public ShapeAuraTicker(CastingSpell spell, Player player, Element element, StoredMana charge) {
            this.spell = spell;
            this.player = player;
            this.element = element;
            this.charge = charge;
            this.runTaskTimer(Mage.instance, 0, 1);
        }

        @Override
        public void run() {
            this.tick++;
            this.element.spawnParticles(player.getLocation(), this.charge.getAmount() / 2);
            if (tick % 5 == 0) {
                this.element.applyPassive(spell, player.getLocation(), this.charge.getAmount());
                double used = Math.max(Math.log(this.charge.getAmount()), 1);
                this.charge.setAmount(Math.max(this.charge.getAmount() - used, 0));
                if (this.charge.getAmount() < 10) {
                    this.charge.setAmount(0);
                    this.cancel();
                }
            }
        }
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Map<Class, Object> args = spell.getSpellStack().popSomeUnique(StoredMana.class, Element.class);
        StoredMana spellCharge = (StoredMana) args.get(StoredMana.class);
        Element element = (Element) args.get(Element.class);

        if (element == null) element = new Pure();
        if (spellCharge == null) spellCharge = new StoredMana(0);
        spellCharge.setAmount(spellCharge.getAmount() + 10);

        new ShapeAuraTicker(spell, spell.getPlayer(), element, spellCharge);
        return null;
    }
}
