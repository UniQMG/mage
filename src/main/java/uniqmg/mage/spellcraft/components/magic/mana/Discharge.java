package uniqmg.mage.spellcraft.components.magic.mana;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

public class Discharge implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new Discharge();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        StoredMana storedMana = (StoredMana) spell.getSpellStack().popStack(StoredMana.class);
        spell.getCaster().setMana(spell.getCaster().getMana() + storedMana.getAmount());
        storedMana.setAmount(0);
        return null;
    }
}
