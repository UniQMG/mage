package uniqmg.mage.spellcraft.components.magic.elements;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.components.magic.Element;
import uniqmg.mage.util.Util;

public class Air implements SpellComponent, Element {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Air();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(this);
        return null;
    }

    @Override
    public void spawnParticles(Location location, double power) {
        double size = Math.min(Math.sqrt(power / 100) + 1, 2) * 0.25;
        int particles = (int) Math.min(power/5 + 1, 100);
        Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(255, 200, 0), 1);
        location.getWorld().spawnParticle(Particle.REDSTONE, location, particles, size, size, size, dustOptions);
    }

    @Override
    public void applyPassive(CastingSpell spell, Location location, double power) {
        for (int i = 0; i < 5; i++) {
            Block block = Util.randomNearby(location, 5).getBlock();
            if (!block.getType().isSolid() && block.getType() != Material.AIR) {
                this.spawnParticles(block.getLocation(), power/10);
                block.breakNaturally();
            }
        }
    }

    @Override
    public void applyActive(CastingSpell spell, Location location, double power) {
        for (Entity ent: Util.entitiesNearby(location, Math.min(power/25 + 1, 10))) {
            Vector diff = ent.getLocation().subtract(location).toVector();

            if (diff.length() == 0) continue;
            double mag = Math.max(diff.length(), 1);
            diff.normalize().multiply(power/50 * 1/mag);
            ent.setVelocity(ent.getVelocity().add(diff));
        }
    }

    @Override
    public void applyActive(CastingSpell spell, Entity entity, double power) {
        this.applyActive(spell, entity.getLocation(), power);
    }

    @Override
    public void applySpecial(CastingSpell spell, Location location, double power) {
    }
}
