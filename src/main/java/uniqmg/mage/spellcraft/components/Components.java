package uniqmg.mage.spellcraft.components;

import java.util.HashMap;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginManager;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.components.caster.*;
import uniqmg.mage.spellcraft.components.comparison.*;
import uniqmg.mage.spellcraft.components.control.*;
import uniqmg.mage.spellcraft.components.debug.DumpStack;
import uniqmg.mage.spellcraft.components.debug.Print;
import uniqmg.mage.spellcraft.components.entity.Health;
import uniqmg.mage.spellcraft.components.entity.player.inventory.*;
import uniqmg.mage.spellcraft.components.interrupts.*;
import uniqmg.mage.spellcraft.components.item.Infuse;
import uniqmg.mage.spellcraft.components.magic.shapes.ShapeAura;
import uniqmg.mage.spellcraft.components.magic.shapes.ShapeBall;
import uniqmg.mage.spellcraft.components.magic.shapes.ShapeFocus;
import uniqmg.mage.spellcraft.components.magic.elements.Air;
import uniqmg.mage.spellcraft.components.magic.elements.Earth;
import uniqmg.mage.spellcraft.components.magic.elements.Fire;
import uniqmg.mage.spellcraft.components.magic.elements.Water;
import uniqmg.mage.spellcraft.components.magic.mana.Charge;
import uniqmg.mage.spellcraft.components.magic.mana.Discharge;
import uniqmg.mage.spellcraft.components.magic.mana.GetCharge;
import uniqmg.mage.spellcraft.components.magic.mana.Recharge;
import uniqmg.mage.spellcraft.components.manipulation.*;
import uniqmg.mage.spellcraft.components.syntax.*;
import uniqmg.mage.spellcraft.components.world.*;
import uniqmg.mage.spellcraft.components.entity.player.*;

public class Components {
    private static HashMap<String, Class<? extends SpellComponent>> components = new HashMap<>();
    private static HashMap<String, String> componentAliases = new HashMap<>();
    private Components() {}

    private static String resolveName(String tokenName) {
        if (componentAliases.containsKey(tokenName))
            return componentAliases.get(tokenName);

        if (components.containsKey(tokenName))
            return tokenName;

        return null;
    }

    /**
     * Registers a new permission for a component name. The name is registered as
     * <code>mage.component.{componentName}</code>
     * @param componentName the name of the component.
     */
    private static void registerComponentPermission(String componentName) {
        Permission perm = new Permission(
            "mage.component." + componentName,
            "Allows the player to compile spells with the " + componentName + " component.",
            PermissionDefault.TRUE
        );
        perm.addParent("mage.component.*", true);
        Mage.instance.getServer().getPluginManager().addPermission(perm);
    }

    public static void registerComponent(String name, Class<? extends SpellComponent> clazz, String... aliases) {
        components.put(name, clazz);
        registerComponentPermission(name);
        for (String altname: aliases)
            componentAliases.put(altname, name);
    }

    public static boolean hasPermission(org.bukkit.entity.Player player, String tokenName) {
        return player.hasPermission("mage.component." + tokenName);
    }

    public static Class<? extends SpellComponent> get(String tokenName) {
        return components.get(resolveName(tokenName.toLowerCase()));
    }

    static {
        // caster
        registerComponent("mana", Mana.class);
        registerComponent("player", Player.class);

        // comparison
        registerComponent("equals", Equals.class, "=");
        registerComponent("greater", Greater.class, ">");
        registerComponent("less", Less.class, "<");
        registerComponent("not", Not.class, "!");

        // control
        registerComponent("call", Call.class);
        registerComponent("if", If.class);
        registerComponent("jump", Jump.class);
        registerComponent("overclock", Overclock.class);
        registerComponent("pop", Pop.class);
        registerComponent("push", Push.class);
        registerComponent("return", Return.class);
        registerComponent("sleep", Sleep.class);
        registerComponent("stack", Stack.class);
        registerComponent("terminate", Terminate.class);

        // interrupts
        registerComponent("glideend", GlideEnd.class);
        registerComponent("swimend", SwimEnd.class);
        registerComponent("exhausted", Exhausted.class);
        registerComponent("unhanded", Unhanded.class);
        registerComponent("used", Used.class);

        // item
        registerComponent("infuse", Infuse.class);
        // magic
        registerComponent("ball", ShapeBall.class);
        registerComponent("aura", ShapeAura.class);
        registerComponent("focus", ShapeFocus.class);
        // magic.elements
        registerComponent("earth", Earth.class);
        registerComponent("water", Water.class);
        registerComponent("fire", Fire.class);
        registerComponent("air", Air.class);
        // magic.mana
        registerComponent("charge", Charge.class);
        registerComponent("discharge", Discharge.class);
        registerComponent("getcharge", GetCharge.class);
        registerComponent("recharge", Recharge.class);

        // manipulation
        registerComponent("add", Add.class, "+");
        registerComponent("concat", Concat.class, "..");
        registerComponent("copy", Copy.class);
        registerComponent("discard", Discard.class);
        registerComponent("divide", Multiply.class, "/");
        registerComponent("multiply", Multiply.class, "*");
        registerComponent("normalize", Normalize.class);
        registerComponent("rotate", Rotate.class);
        registerComponent("sub", Sub.class, "-");

        // syntax
        registerComponentPermission("label"); // Label
        registerComponentPermission("literal"); // FixedValue
        registerComponentPermission("saveVariable"); // SaveVariable
        registerComponentPermission("restoreVariable"); // RestoreVariable
        registerComponentPermission("saveGlobal"); // SaveGlobalVariable
        registerComponentPermission("restoreGlobal"); // RestoreGlobalVariable

        // entity
        registerComponent("health", Health.class);
        // entity.player
        registerComponent("aim", Aim.class);
        registerComponent("aimnormal", AimNormal.class);
        registerComponent("aimvector", AimVector.class);
        registerComponent("eyes", Eyes.class);
        registerComponent("position", Position.class);
        registerComponent("sneaking", Sneaking.class);
        // entity.player.inventory
        registerComponent("hand", Hand.class);
        registerComponent("head", Head.class);
        registerComponent("chest", Chest.class);
        registerComponent("legs", Legs.class);
        registerComponent("feet", Feet.class);

        // world
        registerComponent("break", BreakBlock.class);

        // debug
        registerComponent("print", Print.class);
        registerComponent("dumpstack", DumpStack.class);
    }
}
