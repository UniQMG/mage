package uniqmg.mage.spellcraft.components.debug;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.SpellStack;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

import java.util.Stack;

public class DumpStack implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new DumpStack();
    }

    private void printStackToPlayer(SpellStack stack, Player player, Stack<Object> history, int indent) {
        String sIndent = new String(new char[indent]).replace("\0", "-");
        String prefix = String.format(
            "%sDumpStack%s%s",
            ChatColor.DARK_GRAY,
            ChatColor.WHITE,
            sIndent
        );

        if (stack.stack.size() == 0) {
            player.sendMessage(String.format(
                "%s> %s%s",
                prefix,
                ChatColor.GOLD,
                "Empty stack"
            ));
            return;
        }

        for (Object object: stack.stack) {
            if (history.indexOf(object) != -1) object = String.format(
                "%s%s[%srecursive%s]%s %s%s",
                ChatColor.BOLD,
                ChatColor.GRAY,
                ChatColor.DARK_GREEN,
                ChatColor.GRAY,
                ChatColor.RESET,
                ChatColor.GREEN,
                object
            );
            player.sendMessage(String.format(
                "%s> %s%s",
                prefix,
                ChatColor.GREEN,
                object
            ));
            if (object instanceof SpellStack) {
                history.push(object);
                printStackToPlayer((SpellStack) object, player, history, indent + 1);
                history.pop();
            }
        }
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = spell.getMagiPlayer().getPlayer();
        this.printStackToPlayer(spell.getSpellStack(), player, new Stack<>(), 0);
        return null;
    }
}
