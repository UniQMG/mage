package uniqmg.mage.spellcraft.components.debug;

import org.bukkit.ChatColor;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Print implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Print();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getPlayer().sendMessage(String.format(
            "%s%s[%sSpell%s]%s %s",
            ChatColor.BOLD,
            ChatColor.GRAY,
            ChatColor.WHITE,
            ChatColor.GRAY,
            ChatColor.RESET,
            spell.getSpellStack().popStack(Object.class)
        ));
        return null;
    }
}
