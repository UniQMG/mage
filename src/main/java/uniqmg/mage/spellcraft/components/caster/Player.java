package uniqmg.mage.spellcraft.components.caster;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Player implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Player();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(spell.getMagiPlayer().getPlayer());
        return null;
    }
}
