package uniqmg.mage.spellcraft.components.caster;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Mana implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Mana();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack((int) spell.getCaster().getMana());
        spell.getSpellStack().pushStack((int) spell.getCaster().getMaximumMana());
        return null;
    }
}
