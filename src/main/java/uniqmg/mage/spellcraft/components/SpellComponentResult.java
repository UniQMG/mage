package uniqmg.mage.spellcraft.components;

public enum SpellComponentResult {
    /**
     * Causes the instruction counter to not increment, repeating this instruction
     * unless the instruction counter was modified externally. Should be returned
     * when modifying the instruction counter.
     */
    REPEAT_INSTRUCTION,
    /**
     * Equivalent to null, causes the instruction counter to increment
     * after running this component
     */
    CONTINUE,
    /**
     * Indicates the script should immediately exit (e.g. if the spell exhausts
     * an external resource such as mana)
     */
    TERMINATE
}
