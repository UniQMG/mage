package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class If implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new If();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        boolean cond = (boolean) spell.getSpellStack().popStack(Boolean.class);
        if (!cond) {
            spell.setInstruction(spell.getInstruction()+1);
        }
        return null;
    }
}
