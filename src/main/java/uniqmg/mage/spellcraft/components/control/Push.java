package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.SpellStack;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Push implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Push();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        SpellStack stack = (SpellStack) spell.getSpellStack().popStack(SpellStack.class);
        Object value = spell.getSpellStack().popStack(Object.class);
        stack.pushStack(value);
        return null;
    }
}
