package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Overclock implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new Overclock();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        int speed = (int) spell.getSpellStack().popStack(Integer.class);
        spell.setSpeed(speed);
        return null;
    }
}
