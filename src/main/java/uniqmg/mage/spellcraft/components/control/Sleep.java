package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Sleep implements SpellComponent {
    private boolean setup = false;
    private int ticks;
    private int elapsed;

    @Override
    public double getCost(CastingSpell spell) {
        return 0.1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Sleep();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        if (!setup) {
            ticks = (int) spell.getSpellStack().popStack(Integer.class);
            elapsed = 0;
            setup = true;
        }

        this.elapsed++;
        if (this.elapsed > this.ticks) {
            this.setup = false;
            return SpellComponentResult.CONTINUE;
        }

        return SpellComponentResult.REPEAT_INSTRUCTION;
    }
}
