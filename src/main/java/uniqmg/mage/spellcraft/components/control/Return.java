package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class Return implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Return();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        if (spell.callstack.size() == 0)
            throw new SpellRuntimeException("Attempted to return but the callstack underflowed.");
        spell.setInstruction(spell.callstack.pop() + 1);
        return SpellComponentResult.REPEAT_INSTRUCTION;
    }
}
