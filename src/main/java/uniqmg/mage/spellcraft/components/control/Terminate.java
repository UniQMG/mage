package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Terminate implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new Terminate();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        return SpellComponentResult.TERMINATE;
    }
}
