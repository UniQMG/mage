package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class Call implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Call();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        String labelName = (String) spell.getSpellStack().popStack(String.class);

        int location = spell.getLabelLocation(labelName);
        if (location == -1)
            throw new SpellRuntimeException("Unknown label " + labelName);

        spell.callstack.push(spell.getInstruction());
        spell.setInstruction(location);
        return null;
    }
}
