package uniqmg.mage.spellcraft.components.control;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.SpellStack;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Stack implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Stack();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        spell.getSpellStack().pushStack(new SpellStack());
        return null;
    }
}
