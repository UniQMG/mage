package uniqmg.mage.spellcraft.components.world;

import org.bukkit.Location;
import org.bukkit.block.Block;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class BreakBlock implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        Location location = (Location) spell.getSpellStack().peekStack(Location.class);
        Block block = location.getWorld().getBlockAt(location);
        return block.getType().getHardness() + 10;
    }

    @Override
    public SpellComponent createCopy() {
        return new BreakBlock();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Location location = (Location) spell.getSpellStack().popStack(Location.class);
        Block block = location.getWorld().getBlockAt(location);
        float hardness = block.getType().getHardness();
        if (hardness == -1.0) return null;
        spell.protection.breakBlock(block);
        return null;
    }
}
