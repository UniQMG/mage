package uniqmg.mage.spellcraft.components.item;

import org.bukkit.inventory.ItemStack;
import uniqmg.mage.caster.InfusedTool;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.StoredMana;

public class Infuse implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Infuse();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        ItemStack item = (ItemStack) spell.getSpellStack().popStack(ItemStack.class);
        StoredMana mana = (StoredMana) spell.getSpellStack().popStack(StoredMana.class);
        InfusedTool tool = new InfusedTool(item);
        tool.setMana(tool.getMana() + mana.getAmount());
        return null;
    }
}
