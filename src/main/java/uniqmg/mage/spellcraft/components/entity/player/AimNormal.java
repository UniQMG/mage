package uniqmg.mage.spellcraft.components.entity.player;

import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class AimNormal implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 0;
    }

    @Override
    public SpellComponent createCopy() {
        return new AimNormal();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = (Player) spell.getSpellStack().popStack(Player.class);
        BlockFace face = player.getTargetBlockFace(20);
        if (face == null) throw new SpellRuntimeException("No aimed block, or out of range.");
        spell.getSpellStack().stack.push(face.getDirection());
        return null;
    }
}
