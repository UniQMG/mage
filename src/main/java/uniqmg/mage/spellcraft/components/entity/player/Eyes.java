package uniqmg.mage.spellcraft.components.entity.player;

import org.bukkit.entity.Player;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Eyes implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Eyes();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = (Player) spell.getSpellStack().popStack(Player.class);
        spell.getSpellStack().pushStack(player.getEyeLocation());
        return null;
    }
}
