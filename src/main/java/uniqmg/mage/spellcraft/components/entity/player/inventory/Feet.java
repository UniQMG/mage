package uniqmg.mage.spellcraft.components.entity.player.inventory;

import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Feet implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Feet();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = (Player) spell.getSpellStack().popStack(Player.class);
        ItemStack item = player.getInventory().getItem(EquipmentSlot.FEET);
        spell.getSpellStack().pushStack(item);
        return null;
    }
}
