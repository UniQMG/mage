package uniqmg.mage.spellcraft.components.entity;

import org.bukkit.entity.LivingEntity;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Health implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Health();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        LivingEntity ent = (LivingEntity) spell.getSpellStack().popStack(LivingEntity.class);
        spell.getSpellStack().pushStack(ent.getHealth());
        return null;
    }
}
