package uniqmg.mage.spellcraft.components.entity.player;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

public class Aim implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    public Aim createCopy() {
        return new Aim();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Player player = (Player) spell.getSpellStack().popStack(Player.class);
        Block targetBlock = player.getTargetBlock(20);
        if (targetBlock == null) throw new SpellRuntimeException("No aimed block, or out of range.");
        spell.getSpellStack().pushStack(targetBlock.getLocation());
        return null;
    }
}
