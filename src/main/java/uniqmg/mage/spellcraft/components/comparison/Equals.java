package uniqmg.mage.spellcraft.components.comparison;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Equals implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Equals();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object left = spell.getSpellStack().popStack(Object.class);
        Object right;
        if (left instanceof TripleTuple) {
            right = spell.getSpellStack().popStack(TripleTuple.class);
        } else {
            right = spell.getSpellStack().popStack(Object.class);
        }
        spell.getSpellStack().pushStack(left.equals(right));
        return null;
    }
}
