package uniqmg.mage.spellcraft.components.comparison;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Less implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 5;
    }

    @Override
    public SpellComponent createCopy() {
        return new Less();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        double a = (double) spell.getSpellStack().popStack(Number.class);
        double b = (double) spell.getSpellStack().popStack(Number.class);
        spell.getSpellStack().pushStack(b < a);
        return null;
    }
}