package uniqmg.mage.spellcraft.components;

import uniqmg.mage.spellcraft.CastingSpell;

public interface SpellComponent {
    /**
     * Gets the execution cost for this component. This is the exact amount of mana needed to
     * run the next application of the component and may change between calls.
     * @return the base mana cost to run this component
     * @param spell the castingspell instance
     */
    double getCost(CastingSpell spell);

    /**
     * Creates a functional copy of the spell component. It is not guaranteed to be identical to the parent,
     * and only implements best-effort determinism.
     * @return a copy of the SpellComponent
     */
    SpellComponent createCopy();

    /**
     * Runs the spell component
     * @param spell the castingspell instance
     * @return the next interpreter action from the result enum
     */
    SpellComponentResult apply(CastingSpell spell);
}
