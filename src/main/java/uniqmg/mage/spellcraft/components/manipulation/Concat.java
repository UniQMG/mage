package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;

public class Concat implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Concat();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object a1 = spell.getSpellStack().popStack(Object.class);
        Object a2 = spell.getSpellStack().popStack(Object.class);
        spell.getSpellStack().pushStack(a2.toString() + a1.toString());
        return null;
    }
}
