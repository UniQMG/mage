package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Sub implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Sub();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object loc1 = spell.getSpellStack().popStack(Number.class);
        if (loc1 instanceof TripleTuple) {
            TripleTuple loc2 = (TripleTuple) spell.getSpellStack().popStack(TripleTuple.class);
            spell.getSpellStack().pushStack(((TripleTuple) loc1).invertedSubtract(loc2).getOriginalForm());
        }

        double num1 = (double) loc1;
        double num2 = (double) spell.getSpellStack().popStack(Number.class);
        spell.getSpellStack().pushStack(num2 - num1);
        return null;
    }
}