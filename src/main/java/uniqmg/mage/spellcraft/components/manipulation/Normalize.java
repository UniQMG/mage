package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Normalize implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Normalize();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        TripleTuple vector = (TripleTuple) spell.getSpellStack().popStack(TripleTuple.class);
        spell.getSpellStack().pushStack(vector.normalize().getOriginalForm());
        return null;
    }
}
