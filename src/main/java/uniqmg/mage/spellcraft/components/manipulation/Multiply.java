package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Multiply implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Multiply();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object loc1 = spell.getSpellStack().popStack(TripleTuple.class, Number.class);
        if (loc1 instanceof TripleTuple) {
            Object loc2 = spell.getSpellStack().popStack(TripleTuple.class, Number.class);
            if (loc2 instanceof TripleTuple) {
                spell.getSpellStack().pushStack(((TripleTuple) loc1).multiply((TripleTuple) loc2).getOriginalForm());
            } else {
                spell.getSpellStack().pushStack(((TripleTuple) loc1).multiply((double) loc2).getOriginalForm());
            }
            return null;
        }

        double num1 = (double) loc1;
        double num2 = (double) spell.getSpellStack().popStack(Number.class);
        spell.getSpellStack().pushStack(num2 * num1);
        return null;
    }
}
