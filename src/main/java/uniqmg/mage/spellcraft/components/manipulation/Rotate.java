package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Rotate implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Rotate();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        TripleTuple vector = (TripleTuple) spell.getSpellStack().popStack(TripleTuple.class);
        double yaw = (double) spell.getSpellStack().popStack(Number.class) * Math.PI / 180d;
        double pitch = (double) spell.getSpellStack().popStack(Number.class) * Math.PI / 180d;
        spell.getSpellStack().pushStack(vector.rotate(pitch, yaw).getOriginalForm());
        return null;
    }
}
