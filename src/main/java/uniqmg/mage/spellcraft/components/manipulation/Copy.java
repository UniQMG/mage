package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellParameterException;

public class Copy implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Copy();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        if (spell.getSpellStack().stack.size() == 0)
            throw new SpellParameterException("Tried to copy variable from stack, but stack underflowed");
        Object obj = spell.getSpellStack().stack.pop();
        spell.getSpellStack().stack.push(obj);
        spell.getSpellStack().stack.push(obj);
        return null;
    }
}
