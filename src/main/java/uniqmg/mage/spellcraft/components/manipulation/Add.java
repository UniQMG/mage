package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Add implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Add();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object loc1 = spell.getSpellStack().popStack(TripleTuple.class);
        if (loc1 instanceof TripleTuple) {
            TripleTuple loc2 = (TripleTuple) spell.getSpellStack().popStack(TripleTuple.class);
            spell.getSpellStack().pushStack(((TripleTuple) loc1).add(loc2).getOriginalForm());
            return null;
        }

        double num1 = (double) loc1;
        double num2 = (double) spell.getSpellStack().popStack(Number.class);
        spell.getSpellStack().pushStack(num1 + num2);
        return null;
    }
}
