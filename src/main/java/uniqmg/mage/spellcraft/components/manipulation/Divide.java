package uniqmg.mage.spellcraft.components.manipulation;

import uniqmg.mage.spellcraft.CastingSpell;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.SpellComponentResult;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;
import uniqmg.mage.spellcraft.types.TripleTuple;

public class Divide implements SpellComponent {
    @Override
    public double getCost(CastingSpell spell) {
        return 1;
    }

    @Override
    public SpellComponent createCopy() {
        return new Divide();
    }

    @Override
    public SpellComponentResult apply(CastingSpell spell) {
        Object loc1 = spell.getSpellStack().popStack(TripleTuple.class, Number.class);
        if (loc1 instanceof TripleTuple) {
            Object loc2 = spell.getSpellStack().popStack(TripleTuple.class, Number.class);
            if (loc2 instanceof TripleTuple) {
                spell.getSpellStack().pushStack(((TripleTuple) loc1).invertedDivide((TripleTuple) loc2).getOriginalForm());
            } else {
                if ((double) loc2 == 0d) throw new SpellRuntimeException("Division by 0");
                spell.getSpellStack().pushStack(((TripleTuple) loc1).multiply(1 / (double) loc2).getOriginalForm());
            }
            return null;
        }

        double num1 = (double) loc1;
        double num2 = (double) spell.getSpellStack().popStack(Number.class);
        if (num1 == 0d) throw new SpellRuntimeException("Division by 0");
        spell.getSpellStack().pushStack(num2 / num1);
        return null;
    }
}
