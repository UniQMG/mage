package uniqmg.mage.spellcraft;

import uniqmg.mage.Mage;
import uniqmg.mage.caster.SpellCaster;
import uniqmg.mage.spellcraft.components.SpellComponent;

import java.util.ArrayList;
import java.util.List;

public class Spell {
    private List<SpellComponent> components;

    public Spell() {
        this(new ArrayList<>());
    }

    public Spell(List<SpellComponent> components) {
        this.components = new ArrayList<>(components);
    }

    public void cast(Mage plugin, SpellCaster player, SpellComponent... prefixedComponents) {
        List<SpellComponent> components = new ArrayList<>();

        for (SpellComponent component: prefixedComponents) {
            SpellComponent copy = component.createCopy();
            if (copy == null) throw new RuntimeException("Copied component is null. Yell at the dev pls.");
            components.add(copy);
        }

        for (SpellComponent component: this.components) {
            SpellComponent copy = component.createCopy();
            if (copy == null) throw new RuntimeException("Copied component is null. Yell at the dev pls.");
            components.add(copy);
        }

        CastingSpell spell = new CastingSpell(this, player, components);
        spell.runTaskTimer(plugin, 0, 1);
    }
}
