package uniqmg.mage.spellcraft.global;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.mage.Mage;
import uniqmg.mage.spellcraft.exception.SpellRuntimeException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GlobalVariableManager {
    private Map<Player, Map<String, ExpirableValue>> globals = new HashMap<>();
    private BukkitRunnable task;

    private Map<String, ExpirableValue> of(Player player) {
        if (!globals.containsKey(player))
            globals.put(player, new HashMap<>());
        return globals.get(player);
    }

    /**
     * Gets a player's global variable by name
     * @throws SpellRuntimeException if the variable doesn't exist or has expired
     * @param player which player's globals to use
     * @param key the name of the variable
     * @return the variable's value
     */
    public Object get(Player player, String key) {
        Map<String, ExpirableValue> vars = this.of(player);

        if (!vars.containsKey(key))
            throw new SpellRuntimeException("Global variable doesn't exist or has expired");

        ExpirableValue global = vars.get(key);
        global.resetLifetime();

        return global.getObject();
    }

    public void put(Player player, String key, Object value) {
        Map<String, ExpirableValue> vars = this.of(player);
        vars.put(key, new ExpirableValue(value));
    }

    private void decay(int ticks) {
        for (Map.Entry<Player, Map<String, ExpirableValue>> entry: globals.entrySet()) {
            Iterator<Map.Entry<String, ExpirableValue>> iter = entry.getValue().entrySet().iterator();
            while (iter.hasNext())
                if (!iter.next().getValue().decay(ticks))
                    iter.remove();
        }
    }

    /**
     * Starts the decay check
     * @param plugin the plugin to run the timer for
     * @param period ticks between checks. This will make the check less accurate but run less often.
     */
    public void startDecayCheck(Mage plugin, int period) {
        if (this.task != null) throw new IllegalStateException("Already started");
        this.task = new BukkitRunnable() {
            @Override
            public void run() {
                decay(period);
            }
        };
        this.task.runTaskTimer(plugin, 0, period);
    }

    /**
     * Checks if the decay check has started. Still returns true even if the check was stopped afterwards.
     * @return if the decay check has started
     */
    public boolean started() {
        return this.task != null;
    }

    /**
     * Stops the decay check
     */
    public void cancel() {
        if (this.task == null) return;
        this.task.cancel();
    }
}
