package uniqmg.mage.spellcraft.global;

import org.bukkit.Location;
import org.bukkit.util.Vector;
import uniqmg.mage.spellcraft.types.context.IntermediateForm;
import uniqmg.mage.spellcraft.types.TripleTuple;
import uniqmg.mage.spellcraft.types.context.Decayable;

class ExpirableValue {
    private Object object;
    private int lifetime;

    public ExpirableValue(Object object) {
        this.object = object;
        this.resetLifetime();
    }

    public Object getObject() {
        return this.object;
    }

    /**
     * Resets the object's lifetime
     */
    public void resetLifetime() {
        this.lifetime = 5 * 60 * 20; // 5 minutes
    }

    /**
     * Decays the object, which causes DecayableVariables to lose
     * efficacy and for the object's lifetime to decrease
     * @param ticks how many ticks of decay to apply
     * @return if the object is still alive
     */
    public boolean decay(int ticks) {
        if (this.object instanceof Decayable) {
            ((Decayable) this.object).decay(ticks);
        } else {
            Decayable var = ExpirableValue.getDecayableWrapper(this.object);
            if (var != null) {
                var.decay(ticks);
                if (var instanceof IntermediateForm) {
                    this.object = ((IntermediateForm) var).getOriginalForm();
                }
            }
        }

        this.lifetime -= ticks;
        return this.lifetime > 0;
    }

    private static Decayable getDecayableWrapper(Object obj) {
        if (obj instanceof Location)
            return new TripleTuple((Location) obj);

        if (obj instanceof Vector)
            return new TripleTuple((Vector) obj);

        return null;
    }
}