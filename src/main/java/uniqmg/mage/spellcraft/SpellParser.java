package uniqmg.mage.spellcraft;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import uniqmg.mage.Mage;
import uniqmg.mage.caster.MagiPlayer;
import uniqmg.mage.spellcraft.components.Components;
import uniqmg.mage.spellcraft.components.SpellComponent;
import uniqmg.mage.spellcraft.components.syntax.*;
import uniqmg.mage.util.protection.Protect;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpellParser {
    private MagiPlayer player;

    public SpellParser(MagiPlayer player) {
        this.player = player;
    }

    public static boolean isSpellbook(ItemStack item) {
        if (item == null) return false;
        if (item.getType() != Material.WRITTEN_BOOK && item.getType() != Material.WRITABLE_BOOK) return false;

        BookMeta meta = (BookMeta) item.getItemMeta();
        return isSpellbook(meta);
    }

    public static boolean isSpellbook(BookMeta meta) {
        if (meta.getPageCount() == 0) return false;

        String page = meta.getPage(1);
        return page.startsWith("spell");
    }

    public Spell parse(ItemStack item) {
        if (!SpellParser.isSpellbook(item)) return null;
        return this.parse((BookMeta) item.getItemMeta());
    }

    public Spell parse(BookMeta meta) {
        StringBuilder spell = new StringBuilder();
        for (String s: meta.getPages()) {
            spell.append(s);
            spell.append('\n');
        }
        return this.parse(spell);
    }

    public Spell parse(String spell) {
        return this.parse(new StringBuilder(spell));
    }

    private static final Pattern[] tokenExtractors = {
        Pattern.compile("^§[0-9a-f]"),
        Pattern.compile("^(-g?>|<g?-)\\w+"),
        Pattern.compile("^-?[0-9]+(\\.[0-9]*)?"),
        Pattern.compile("^:\\w+"),
        Pattern.compile("^\\w+"),
        Pattern.compile("^\".+?(?<!\\\\)\""),
        Pattern.compile("^[<=>!+\\-*/]"),
        Pattern.compile("^..")
    };

    private void printPermissionError(String token) {
        this.player.getPlayer().sendMessage(String.format(
            "%sYou aren't allowed to use %s%s",
            ChatColor.RED,
            ChatColor.BOLD,
            token
        ));
    }

    /**
     * Parses a spell, returning a Spell object if successful or null if not. Errors will be reported to the player.
     * Spell parsing may fail because of:
     *   * Lacking permissions to cast spells
     *   * Lacking permission to use a component
     *   * An unknown component name
     *   * A syntax error
     * @param spell the spell text
     * @return the parsed spell
     */
    public Spell parse(StringBuilder spell) {
        List<SpellComponent> components = new ArrayList<>();
        int position = 0;

        if (!player.getPlayer().hasPermission("mage.cast")) {
            player.getPlayer().sendMessage(ChatColor.RED + "You don't have permission to cast spells");
            return null;
        }

        if (!new Protect(Mage.instance, player.getPlayer()).canCast()) {
            player.getPlayer().sendMessage(ChatColor.RED + "You don't have permission to cast spells here");
            return null;
        }

        // Tokenize
        Queue<String> tokens = new ArrayDeque<>();
        consumerLoop: while (true) {
            for (Pattern pattern: tokenExtractors) {
                Matcher matcher = pattern.matcher(spell);
                if (matcher.find()) {
                    String matched = matcher.group();
                    position += matched.length();
                    tokens.add(matched);

                    System.out.println("Token: " + matched);

                    spell.delete(0, matched.length());
                    while (
                        spell.length() > 0 && (
                            spell.charAt(0) == ' ' ||
                            spell.charAt(0) == '\t' ||
                            spell.charAt(0) == '\n' ||
                            spell.charAt(0) == '\r'
                        )
                    ) spell.delete(0, 1);
                    if (spell.length() == 0) break consumerLoop;
                    continue consumerLoop;
                }
            }
            this.player.getPlayer().sendMessage(ChatColor.RED + "Unexpected character at position " + position);
            return null;
        }

        if (tokens.size() == 0 || !tokens.remove().equals("spell")) {
            this.player.getPlayer().sendMessage(ChatColor.RED + "Spellbook is empty or has incorrect header");
            return null;
        }

        // Assemble AST. It's linear. abstract syntax line? ASL. oh. whatever. assemble runnable object formation. ROF!
        while (tokens.size() > 0) {
            String token = tokens.remove();
            if (token.charAt(0) == '§') continue;

            if (token.charAt(0) == ':') {
                if (!Components.hasPermission(player.getPlayer(), "label")) {
                    this.printPermissionError("label");
                    return null;
                }
                components.add(new Label(token.substring(1)));
                continue;
            }

            if (token.startsWith("\"")) {
                if (!Components.hasPermission(player.getPlayer(), "literal")) {
                    this.printPermissionError("literal");
                    return null;
                }
                String str = token.substring(1, token.length()-1).replaceAll("&([0-9a-f])", "§$1");
                components.add(new FixedValue<>(str));
                continue;
            }

            if (token.startsWith("->")) {
                if (!Components.hasPermission(player.getPlayer(), "saveVariable")) {
                    this.printPermissionError("saveVariable");
                    return null;
                }
                components.add(new SaveVariable(token.substring(2)));
                continue;
            }

            if (token.startsWith("<-")) {
                if (!Components.hasPermission(player.getPlayer(), "restoreVariable")) {
                    this.printPermissionError("restoreVariable");
                    return null;
                }
                components.add(new RestoreVariable(token.substring(2)));
                continue;
            }

            if (token.startsWith("-g>")) {
                if (!Components.hasPermission(player.getPlayer(), "saveGlobal")) {
                    this.printPermissionError("saveGlobal");
                    return null;
                }
                components.add(new SaveGlobalVariable(token.substring(3)));
                continue;
            }

            if (token.startsWith("<g-")) {
                if (!Components.hasPermission(player.getPlayer(), "restoreGlobal")) {
                    this.printPermissionError("restoreGlobal");
                    return null;
                }
                components.add(new RestoreGlobalVariable(token.substring(3)));
                continue;
            }

            boolean isNumber = (
                token.charAt(0) >= '0' &&
                token.charAt(0) <= '9'
            ) || (
                token.length() > 1 &&
                token.charAt(0) == '-' &&
                token.charAt(1) >= '0' &&
                token.charAt(1) <= '9'
            );
            if (isNumber && Components.hasPermission(player.getPlayer(), "literal")) {
                components.add(new FixedValue<>(Integer.parseInt(token)));
                continue;
            }

            Class<? extends SpellComponent> componentClass = Components.get(token);
            if (componentClass == null) {
                this.player.getPlayer().sendMessage(String.format(
                    "%sUnknown command: %s%s",
                    ChatColor.RED,
                    ChatColor.BOLD,
                    token
                ));
                return null;
            }

            if (!Components.hasPermission(player.getPlayer(), token)) {
                this.player.getPlayer().sendMessage(String.format(
                    "%sYou aren't allowed to use %s%s",
                    ChatColor.RED,
                    ChatColor.BOLD,
                    token
                ));
                return null;
            }

            try {
                components.add(componentClass.newInstance());
            } catch (InstantiationException | IllegalAccessException ex) {
                this.player.getPlayer().sendMessage(String.format(
                    "%s%s%s%s%s cannot be used directly",
                    ChatColor.RED,
                    ChatColor.UNDERLINE,
                    token,
                    ChatColor.RESET,
                    ChatColor.RED
                ));
                return null;
            }
        }

        if (components.size() == 0) {
            this.player.getPlayer().sendMessage("Empty spell");
            return null;
        }

        return new Spell(components);
    }
}
