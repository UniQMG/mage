package uniqmg.mage.spellcraft.exception;

public class SpellInternalException extends SpellRuntimeException {
    public SpellInternalException(String message) {
        super(message);
    }
}
