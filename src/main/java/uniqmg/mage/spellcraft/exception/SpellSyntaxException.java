package uniqmg.mage.spellcraft.exception;

public class SpellSyntaxException extends SpellRuntimeException {
    public SpellSyntaxException(String message) {
        super(message);
    }
}
