package uniqmg.mage.spellcraft.exception;

public class SpellRuntimeException extends RuntimeException {
    public SpellRuntimeException(String message) {
        super(message);
    }
}
