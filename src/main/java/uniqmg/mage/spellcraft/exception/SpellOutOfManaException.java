package uniqmg.mage.spellcraft.exception;

public class SpellOutOfManaException extends SpellRuntimeException {
    public SpellOutOfManaException(String message) {
        super(message);
    }

    public SpellOutOfManaException() {
        this("Mana exhausted");
    }
}
