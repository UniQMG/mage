package uniqmg.mage.spellcraft.exception;

public class SpellStackUnderflow extends SpellRuntimeException {
    public SpellStackUnderflow(String message) {
        super(message);
    }
}
