package uniqmg.mage.spellcraft.exception;

public class SpellParameterException extends SpellRuntimeException {
    public SpellParameterException(String message) {
        super(message);
    }
}
