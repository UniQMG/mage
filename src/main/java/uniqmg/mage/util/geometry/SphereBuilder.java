package uniqmg.mage.util.geometry;

import org.bukkit.Location;

public class SphereBuilder {
    private double radius;
    private Location location;
    private int x;
    private int y;
    private int z;

    public SphereBuilder(Location center, double radius) {
        this.radius = radius;
        this.x = (int) Math.floor(-radius);
        this.y = (int) Math.floor(-radius);
        this.z = (int) Math.floor(-radius);
        this.location = center;
    }

    public static double radius(double volume) {
        return Math.max(Math.cbrt(0.75 * volume / Math.PI), 0);
    }

    public Location next() {
        while (true) {
            x++;
            if (x > Math.ceil(radius)) {
                this.x = (int) Math.floor(-radius);
                y++;
            }
            if (y > Math.ceil(radius)) {
                this.y = (int) Math.floor(-radius);
                z++;
            }
            if (z > Math.ceil(radius)) {
                return null;
            }
            double distance = Math.sqrt(
                Math.pow(x, 2) +
                Math.pow(y, 2) +
                Math.pow(z, 2)
            );
            if (distance < radius)
                return location.clone().add(x, y, z);
        }
    }
}
