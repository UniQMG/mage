package uniqmg.mage.util.protection;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import uniqmg.mage.Mage;

public class WorldGuardIntegration implements ProtectionMethod {
    private static StateFlag ALLOW_SPELLS;
    private static StateFlag ALLOW_SPELL_DAMAGE;

    public static void registerWorldguardFlags() {
        try {
            ALLOW_SPELLS = new StateFlag("mage-allow-spells", true);
            ALLOW_SPELL_DAMAGE = new StateFlag("mage-allow-spell-damage", false);

            FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
            registry.register(ALLOW_SPELLS);
            registry.register(ALLOW_SPELL_DAMAGE);
        } catch (FlagConflictException ex) {
            Mage.instance.getLogger().severe(
                "WorldGuard custom flag conflict - disable integration or remove the offending plugins. " +
                "Flags: 'mage-allow-spells', 'mage-allow-spell-damage'. Spells will not be castable"
            );
            Mage.instance.getLogger().severe("Spells will not be castable");
        }
    }

    @Override
    public boolean canCast(Player player) {
        if (ALLOW_SPELLS == null || ALLOW_SPELL_DAMAGE == null)
            return false;

        LocalPlayer localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionQuery query = container.createQuery();
        Location loc = BukkitAdapter.adapt(player.getLocation());

        boolean canBypass = WorldGuard
                .getInstance()
                .getPlatform()
                .getSessionManager()
                .hasBypass(localPlayer, localPlayer.getWorld());
        if (canBypass) return true;

        return query.testState(loc, localPlayer, ALLOW_SPELLS);
    }

    @Override
    public boolean canModify(Player player, Block block) {
        if (ALLOW_SPELLS == null || ALLOW_SPELL_DAMAGE == null)
            return false;

        LocalPlayer localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionQuery query = container.createQuery();
        Location loc = BukkitAdapter.adapt(block.getLocation());

        ApplicableRegionSet regions = query.getApplicableRegions(loc);
        if (regions.size() == 0) return true;

        return query.testState(loc, localPlayer, ALLOW_SPELL_DAMAGE);
    }
}
