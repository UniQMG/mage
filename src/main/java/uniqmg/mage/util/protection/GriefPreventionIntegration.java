package uniqmg.mage.util.protection;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.ClaimPermission;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import uniqmg.mage.Mage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GriefPreventionIntegration implements ProtectionMethod {
    private Method hasExplicitPermission;

    public GriefPreventionIntegration() {
        try {
            hasExplicitPermission = Claim.class.getDeclaredMethod(
                "hasExplicitPermission",
                Player.class,
                ClaimPermission.class
            );
            hasExplicitPermission.setAccessible(true);
        } catch (NoSuchMethodException ex) {
            Mage.instance.getLogger().severe("GriefPrevention integration broke: " + ex);
            Mage.instance.getLogger().severe("Spells will be unable to break blocks.");
            hasExplicitPermission = null;
        }
    }

    @Override
    public boolean canCast(Player player) {
        return true;
    }

    @Override
    public boolean canModify(Player player, Block block) {
        if (hasExplicitPermission == null)
            return false;

        try {
            Claim claim = GriefPrevention.instance.dataStore.getClaimAt(
                block.getLocation(), false, null
            );
            if (claim == null) return true;
            if (claim.ownerID == player.getUniqueId()) return true;
            return (boolean) hasExplicitPermission.invoke(claim, player, ClaimPermission.Build);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Mage.instance.getLogger().warning("GriefPreventionIntegration integration broke: " + ex);
            return false;
        }
    }
}
