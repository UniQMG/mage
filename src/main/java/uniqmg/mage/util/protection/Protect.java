package uniqmg.mage.util.protection;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import uniqmg.mage.Mage;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides grief prevention api checks for spells, hooking into several possible
 * plugins and unifying them into a single interface.
 */
public class Protect {
    private final Mage plugin;
    private final Player player;
    private final List<ProtectionMethod> methods;

    /**
     * Creates a new protection instance
     * @param plugin the plugin using this protection instance
     * @param actor the player responsible for changes who requires permission
     */
    public Protect(Mage plugin, Player actor) {
        this.plugin = plugin;
        this.player = actor;
        this.methods = new ArrayList<>();

        PluginManager manager = plugin.getServer().getPluginManager();

        if (plugin.getConfig().getBoolean("protection.worldguard", false))
            if (manager.getPlugin("WorldGuard") != null)
                this.methods.add(new WorldGuardIntegration());

        if (plugin.getConfig().getBoolean("protection.griefprevention", false))
            if (manager.getPlugin("GriefPrevention") != null)
                this.methods.add(new GriefPreventionIntegration());

        if (plugin.getConfig().getBoolean("protection.simulate", false))
            this.methods.add(new SimulateBlockBreak());

        if (plugin.getConfig().getBoolean("protection.all", false))
            this.methods.add(new DenyBreak());
    }

    /**
     * Sets a block, first checking if permission is granted
     * @param block the block to replace with the given material
     * @param material the material to set the block to
     */
    public void setBlock(Block block, Material material) {
        if (!this.hasPermission(block)) return;
        block.setType(material);
    }

    /**
     * Breaks a block naturally, first checking if permission is granted
     * @param block the block to break
     */
    public void breakBlock(Block block) {
        if (!this.hasPermission(block)) return;
        block.breakNaturally();
    }

    /**
     * Checks if the player can cast based on their current location
     * @return whether the player can currently cast spells
     */
    public boolean canCast() {
        for (ProtectionMethod method: this.methods) {
            if (!method.canCast(player)) {
                Mage.instance.getLogger().info(String.format(
                    "%s failed permission check for player %s casting",
                    method.getClass().getSimpleName(),
                    player
                ));
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if permission is granted to edit a given block
     * @param block the block to edit
     * @return whether permission is granted to edit the block
     */
    public boolean hasPermission(Block block) {
        for (ProtectionMethod method: this.methods) {
            if (!method.canModify(player, block)) {
                Mage.instance.getLogger().info(String.format(
                    "%s failed permission check for player %s modifying block %s",
                    method.getClass().getSimpleName(),
                    player,
                    block
                ));
                return false;
            }
        }
        return true;
    }
}
