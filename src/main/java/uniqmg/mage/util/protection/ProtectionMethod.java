package uniqmg.mage.util.protection;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface ProtectionMethod {
    boolean canCast(Player player);
    boolean canModify(Player player, Block block);
    // TODO: canInteract(Entity entity) for entity protection
}
