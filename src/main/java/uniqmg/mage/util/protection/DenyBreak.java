package uniqmg.mage.util.protection;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class DenyBreak implements ProtectionMethod {
    @Override
    public boolean canCast(Player player) {
        return true;
    }

    @Override
    public boolean canModify(Player player, Block block) {
        return false;
    }
}
