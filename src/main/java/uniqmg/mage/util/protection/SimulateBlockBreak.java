package uniqmg.mage.util.protection;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

public class SimulateBlockBreak implements ProtectionMethod {
    @Override
    public boolean canCast(Player player) {
        return true;
    }

    @Override
    public boolean canModify(Player player, Block block) {
        BlockBreakEvent evt = new BlockBreakEvent(block, player);
        Bukkit.getPluginManager().callEvent(evt);
        return !evt.isCancelled();
    }
}
