package uniqmg.mage.util;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collection;

public class Util {
    private Util() {}

    public static void potionAoE(
            PotionEffectType type,
            Location location,
            double radius,
            int duration,
            int amplifier,
            double chance
    ) {
        Collection<Entity> entities = location.getWorld().getNearbyEntities(
            location, radius/2, radius/2, radius/2
        );
        for (Entity entity: entities) {
            if (entity instanceof LivingEntity) {
                if (Math.random() > chance) continue;
                PotionEffect effect = new PotionEffect(type, duration, amplifier);
                ((LivingEntity) entity).addPotionEffect(effect);
            }
        }
    }

    public static Collection<Entity> entitiesNearby(Location location, double radius) {
        return location.getWorld().getNearbyEntities(location, radius/2, radius/2, radius/2);
    }

    public static void potionAoE(PotionEffectType type, Location location, double radius, double chance) {
        Util.potionAoE(type, location, radius, 100, 0, chance);
    }

    public static void potionAoE(PotionEffectType type, Location location, double radius) {
        Util.potionAoE(type, location, radius, 1.0);
    }

    public static Location randomNearby(Location location, double radius) {
        return location.clone().add(
            (Math.random()-0.5) * radius,
            (Math.random()-0.5) * radius,
            (Math.random()-0.5) * radius
        );
    }
}
