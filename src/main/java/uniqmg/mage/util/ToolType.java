package uniqmg.mage.util;

import org.bukkit.Material;

import static org.bukkit.Material.WOODEN_SWORD;

public class ToolType {
    public enum TypeClass {
        OTHER, WEAPON, TOOL, ARMOR
    }

    public enum MaterialClass {
        NONE, DIAMOND, GOLD, IRON, WOOD, OTHER
    }

    public enum ToolCategory {
        OTHER, SWORD, BOW, FISHING_ROD, PICKAXE, SHOVEL, AXE, HOE,
        HELMET, ELYTRA, CHESTPLATE, LEGGINGS, BOOTS
    }

    public static MaterialClass classOf(Material material) {
        switch (material) {
            case DIAMOND_SWORD:
            case DIAMOND_PICKAXE:
            case DIAMOND_SHOVEL:
            case DIAMOND_AXE:
            case DIAMOND_HOE:
            case DIAMOND_HELMET:
            case DIAMOND_CHESTPLATE:
            case DIAMOND_LEGGINGS:
            case DIAMOND_BOOTS:
                return MaterialClass.DIAMOND;

            case GOLDEN_SWORD:
            case GOLDEN_PICKAXE:
            case GOLDEN_SHOVEL:
            case GOLDEN_AXE:
            case GOLDEN_HOE:
            case GOLDEN_HELMET:
            case GOLDEN_CHESTPLATE:
            case GOLDEN_LEGGINGS:
            case GOLDEN_BOOTS:
                return MaterialClass.GOLD;

            case IRON_SWORD:
            case IRON_PICKAXE:
            case IRON_SHOVEL:
            case IRON_AXE:
            case IRON_HOE:
            case IRON_HELMET:
            case IRON_CHESTPLATE:
            case IRON_LEGGINGS:
            case IRON_BOOTS:
                return MaterialClass.IRON;

            case WOODEN_SWORD:
            case WOODEN_PICKAXE:
            case WOODEN_SHOVEL:
            case WOODEN_AXE:
            case WOODEN_HOE:
            case BOW:
            case FISHING_ROD:
            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
            case LEATHER_LEGGINGS:
            case LEATHER_BOOTS:
                return MaterialClass.WOOD;

            case ELYTRA:
                return MaterialClass.OTHER;

            default:
                return MaterialClass.NONE;
        }
    }

    public static TypeClass typeOf(Material material) {
        switch (material) {
            case DIAMOND_SWORD:
            case GOLDEN_SWORD:
            case IRON_SWORD:
            case WOODEN_SWORD:

            case BOW:
                return TypeClass.WEAPON;

            case DIAMOND_PICKAXE:
            case GOLDEN_PICKAXE:
            case IRON_PICKAXE:
            case WOODEN_PICKAXE:

            case DIAMOND_SHOVEL:
            case GOLDEN_SHOVEL:
            case IRON_SHOVEL:
            case WOODEN_SHOVEL:

            case DIAMOND_AXE:
            case GOLDEN_AXE:
            case IRON_AXE:
            case WOODEN_AXE:

            case DIAMOND_HOE:
            case GOLDEN_HOE:
            case IRON_HOE:
            case WOODEN_HOE:

            case FISHING_ROD:
                return TypeClass.TOOL;

            case DIAMOND_HELMET:
            case GOLDEN_HELMET:
            case IRON_HELMET:
            case LEATHER_HELMET:

            case DIAMOND_CHESTPLATE:
            case GOLDEN_CHESTPLATE:
            case IRON_CHESTPLATE:
            case LEATHER_CHESTPLATE:

            case DIAMOND_LEGGINGS:
            case GOLDEN_LEGGINGS:
            case IRON_LEGGINGS:
            case LEATHER_LEGGINGS:

            case DIAMOND_BOOTS:
            case GOLDEN_BOOTS:
            case IRON_BOOTS:
            case LEATHER_BOOTS:

            case ELYTRA:
                return TypeClass.ARMOR;

            default:
                return TypeClass.OTHER;
        }
    }

    public static ToolCategory toolCategoryOf(Material material) {
        switch (material) {
            case DIAMOND_SWORD:
            case GOLDEN_SWORD:
            case IRON_SWORD:
            case WOODEN_SWORD:
                return ToolCategory.SWORD;

            case DIAMOND_PICKAXE:
            case GOLDEN_PICKAXE:
            case IRON_PICKAXE:
            case WOODEN_PICKAXE:
                return ToolCategory.PICKAXE;

            case DIAMOND_SHOVEL:
            case GOLDEN_SHOVEL:
            case IRON_SHOVEL:
            case WOODEN_SHOVEL:
                return ToolCategory.SHOVEL;

            case DIAMOND_AXE:
            case GOLDEN_AXE:
            case IRON_AXE:
            case WOODEN_AXE:
                return ToolCategory.AXE;

            case DIAMOND_HOE:
            case GOLDEN_HOE:
            case IRON_HOE:
            case WOODEN_HOE:
                return ToolCategory.HOE;

            case BOW:
                return ToolCategory.BOW;

            case FISHING_ROD:
                return ToolCategory.FISHING_ROD;

            case DIAMOND_HELMET:
            case GOLDEN_HELMET:
            case IRON_HELMET:
            case LEATHER_HELMET:
                return ToolCategory.HELMET;

            case DIAMOND_CHESTPLATE:
            case GOLDEN_CHESTPLATE:
            case IRON_CHESTPLATE:
            case LEATHER_CHESTPLATE:
                return ToolCategory.CHESTPLATE;

            case DIAMOND_LEGGINGS:
            case GOLDEN_LEGGINGS:
            case IRON_LEGGINGS:
            case LEATHER_LEGGINGS:
                return ToolCategory.LEGGINGS;

            case DIAMOND_BOOTS:
            case GOLDEN_BOOTS:
            case IRON_BOOTS:
            case LEATHER_BOOTS:
                return ToolCategory.BOOTS;

            case ELYTRA:
                return ToolCategory.ELYTRA;

            default:
                return ToolCategory.OTHER;
        }
    }
}
