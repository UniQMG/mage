package uniqmg.mage.caster;

public interface SpellCaster {
    /**
     * Attempt to consume a given amount of mana.
     * @param manaCost the mana to consume
     * @return whether the consumption was successful
     */
    default boolean consumeMana(double manaCost) {
        return this.consumeMana(manaCost, false);
    }

    /**
     * Attempts to consume a given amount of mana
     * @param manaCost the mana to consume
     * @param bypassRate whether to ignore the supported mana rate
     * @return whether the consumption was successful
     */
    boolean consumeMana(double manaCost, boolean bypassRate);

    /**
     * Sets the internal reservoir of mana
     * @param mana the new amount of mana
     */
    void setMana(double mana);

    /**
     * Returns the immediately available quantity of mana. consumeMana may be able
     * to consume more mana than this, however.
     * @return the amount of guaranteed immediately available mana.
     */
    double getMana();

    /**
     * Returns the maximum amount of mana storable by this caster. This doesn't represent
     * the maximum amount ever available, and the internal pool may be restored faster than
     * it can be consumed. To get the immediately usable amount, see {@link #getManaRate()}
     * @return the maximum locally channelable mana
     */
    double getMaximumMana();

    /**
     * Returns the amount of usable mana per tick from this source.
     * @return the maximum locally channelable mana
     */
    double getManaRate();

    /**
     * Returns the maximum amount of immediately usable mana
     * @return the maximum amount of immediately usable mana
     */
    default double getUsableMana() {
        return Math.min(this.getMana(), this.getManaRate());
    }
}
