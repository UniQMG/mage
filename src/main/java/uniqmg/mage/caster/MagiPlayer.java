package uniqmg.mage.caster;

import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import uniqmg.mage.Mage;

public class MagiPlayer implements SpellCaster {
    private Player player;
    private BossBar player_bar;
    private BossBar item_bar;
    private double mana;
    private double maxMana;
    private double manaRate;

    private boolean shown;

    public MagiPlayer(Player player) {
        this.player = player;
        this.maxMana = Mage.instance.getConfig().getDouble("player.mana", 1000);
        this.mana = this.maxMana;
        this.manaRate = Mage.instance.getConfig().getDouble("player.rate", 1000);
    }

    public Player getPlayer() {
        return this.player;
    }

    @Override
    public boolean consumeMana(double manaCost, boolean bypassRate) {
        double availableMana = bypassRate ? this.getMana() : Math.min(this.getMana(), this.getManaRate());
        if (manaCost > availableMana) return false;
        this.setMana(this.getMana() - manaCost);
        return true;
    }

    @Override
    public void setMana(double mana) {
        if (mana > this.maxMana) mana = this.maxMana;
        this.mana = mana;
        this.updateManaBar();
    }

    @Override
    public double getMana() {
        return this.mana;
    }

    @Override
    public double getManaRate() {
        return this.manaRate;
    }

    @Override
    public double getMaximumMana() {
        return this.maxMana;
    }

    public ToolCaster getMagicTool() {
        ItemStack item = this.player.getInventory().getItemInMainHand();
        CustomItemTagContainer ctag = item.getItemMeta().getCustomTagContainer();
        if (ctag.hasCustomTag(Mage.instance.mana, ItemTagType.DOUBLE))
            return new ToolCaster(this, item);
        return null;
    }

    public void updateManaBar() {
        this.player_bar.setProgress(this.maxMana == 0 ? 0 : this.mana / this.maxMana);
        this.player_bar.setVisible(this.shown && (this.mana < this.maxMana));

        if (this.mana < this.maxMana * 0.25) player_bar.setColor(BarColor.RED);
        else if (this.mana < this.maxMana * 0.50) player_bar.setColor(BarColor.PINK);
        else player_bar.setColor(BarColor.BLUE);
    }

    @SuppressWarnings("ConstantConditions")
    public void updateItemManaBar(ItemStack item) {
        if (item == null) {
            this.updateItemManaBar(0, 0);
            return;
        }
        ItemMeta meta = item.getItemMeta();
        if (meta == null) {
            this.updateItemManaBar(0, 0);
            return;
        }
        CustomItemTagContainer ctag = meta.getCustomTagContainer();
        if (ctag.hasCustomTag(Mage.instance.mana, ItemTagType.DOUBLE)) {
            this.updateItemManaBar(
                ctag.getCustomTag(Mage.instance.mana, ItemTagType.DOUBLE),
                ctag.getCustomTag(Mage.instance.maxMana, ItemTagType.DOUBLE)
            );
        } else {
            this.updateItemManaBar(0, 0);
        }
    }

    public void updateItemManaBar(double mana, double maxMana) {
        this.item_bar.setProgress(maxMana == 0 ? 0 : mana / maxMana);
        this.item_bar.setVisible(this.shown && maxMana > 0);
        this.item_bar.setColor(mana == 0 ? BarColor.RED : BarColor.BLUE);
    }

    public void updateBar(boolean show) {
        if (this.player_bar == null) {
            this.player_bar = Mage.instance.getServer().createBossBar("Mana", BarColor.BLUE, BarStyle.SEGMENTED_6);
            this.item_bar = Mage.instance.getServer().createBossBar("Tool Mana", BarColor.GREEN, BarStyle.SEGMENTED_6);
            this.player_bar.addPlayer(player);
            this.item_bar.addPlayer(player);
            this.item_bar.setVisible(false);
        }

        this.shown = show;
        this.updateManaBar();
        this.item_bar.setVisible(shown && this.item_bar.isVisible());
    }

    public void setMaxMana(double maxMana) {
        this.maxMana = maxMana;
    }
}
