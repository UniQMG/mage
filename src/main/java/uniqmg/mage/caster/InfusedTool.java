package uniqmg.mage.caster;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import uniqmg.mage.Mage;

public class InfusedTool implements SpellCaster {
    protected ItemStack item;

    public InfusedTool(ItemStack item) {
        this.item = item;
    }

    @Override
    public boolean consumeMana(double manaCost, boolean bypassRate) {
        double mana = bypassRate ? this.getMana() : Math.min(this.getMana(), this.getManaRate());
        // Intentionally subtract mana even if it failed
        this.setMana(this.getMana() - manaCost);
        return mana >= manaCost;
    }

    @Override
    public void setMana(double mana) {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return;
        CustomItemTagContainer ctag = meta.getCustomTagContainer();
        if (!ctag.hasCustomTag(Mage.instance.mana, ItemTagType.DOUBLE)) return;
        double newMana = Math.max(Math.min(mana, this.getMaximumMana()), 0);
        ctag.setCustomTag(Mage.instance.mana, ItemTagType.DOUBLE, newMana);
        item.setItemMeta(meta);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public double getMana() {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return 0;
        CustomItemTagContainer ctag = meta.getCustomTagContainer();
        if (ctag.hasCustomTag(Mage.instance.mana, ItemTagType.DOUBLE))
            return ctag.getCustomTag(Mage.instance.mana, ItemTagType.DOUBLE);
        return 0;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public double getMaximumMana() {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return 0;
        CustomItemTagContainer ctag = meta.getCustomTagContainer();
        if (ctag.hasCustomTag(Mage.instance.maxMana, ItemTagType.DOUBLE))
            return ctag.getCustomTag(Mage.instance.maxMana, ItemTagType.DOUBLE);
        return 0;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public double getManaRate() {
        ItemMeta meta = item.getItemMeta();
        if (meta == null) return this.getMaximumMana();
        CustomItemTagContainer ctag = meta.getCustomTagContainer();
        if (ctag.hasCustomTag(Mage.instance.maxManaRate, ItemTagType.DOUBLE))
            return ctag.getCustomTag(Mage.instance.maxManaRate, ItemTagType.DOUBLE);
        return this.getMaximumMana();
    }
}
