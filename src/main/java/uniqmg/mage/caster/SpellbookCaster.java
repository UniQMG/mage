package uniqmg.mage.caster;

import org.bukkit.inventory.ItemStack;

public class SpellbookCaster implements SpellCaster, ItemCaster {
    public final MagiPlayer player;
    public final ItemStack book;

    public SpellbookCaster(MagiPlayer player, ItemStack book) {
        this.player = player;
        this.book = book;
    }

    @Override
    public boolean consumeMana(double manaCost, boolean bypassRate) {
        return this.player.consumeMana(manaCost, bypassRate);
    }

    @Override
    public void setMana(double mana) {
        this.player.setMana(mana);
    }

    @Override
    public double getMana() {
        return this.player.getMana();
    }

    @Override
    public double getMaximumMana() {
        return this.player.getMaximumMana();
    }

    @Override
    public double getManaRate() {
        return this.player.getManaRate();
    }

    @Override
    public ItemStack getItem() {
        return this.book;
    }
}
