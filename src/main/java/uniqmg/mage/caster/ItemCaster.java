package uniqmg.mage.caster;

import org.bukkit.inventory.ItemStack;

public interface ItemCaster {
    /**
     * Gets the item associated with the casting spell
     * @return the ItemStack associated with the spell
     */
    ItemStack getItem();
}
