package uniqmg.mage.caster;

import org.bukkit.inventory.ItemStack;

public class ToolCaster extends InfusedTool implements ItemCaster {
    public final MagiPlayer player;

    public ToolCaster(MagiPlayer player, ItemStack item) {
        super(item);
        this.player = player;
    }

    @Override
    public void setMana(double mana) {
        super.setMana(mana);
        this.player.updateItemManaBar(this.getMana(), this.getMaximumMana());
    }

    @Override
    public ItemStack getItem() {
        return this.item;
    }
}
