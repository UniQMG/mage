package uniqmg.mage.caster;

/**
 * Proxies request to a SpellCaster, limiting the amount of mana drawn per tick
 * to the source's mana rate. The CasterTickProxy must be reset each tick to count properly.
 */
public class CasterTickProxy implements SpellCaster {
    private SpellCaster source;
    private double manaUsed;
    private double charge;

    /**
     * Creates a new caster tick proxy that draws from the specified source
     * @param source the source to draw from
     */
    public CasterTickProxy(SpellCaster source) {
        this.source = source;
        this.manaUsed = 0;
        this.charge = 0;
    }

    /**
     * Ticks the caster proxy, resetting the amount of mana used this tick.
     */
    public void tick() {
        this.manaUsed = 0;
    }

    public SpellCaster getProxiedCaster() {
        return this.source;
    }

    /**
     * Charges up the proxy, drawing as much mana as possible per tick until the
     * specified limit is reached. This should be used for individual requests
     * that require more than the available mana rate
     * @param charge the limit to charge to
     * @return whether the specified limit has been reached
     */
    public boolean chargeTo(final double charge) {
        double delta = charge - this.charge;

        if (this.getMana() == 0)
            return false;

        if (delta > 0) {
            double maxDelta = Math.min(this.getRateLeft(), delta);
            if (this.source.consumeMana(maxDelta)) {
                this.manaUsed += maxDelta;
                this.charge += maxDelta;
            }
        }

        return this.charge >= charge;
    }

    /**
     * Gets the amount of charged mana ready to burst.
     * @return the amount of charged mana
     */
    public double getCharge() {
        return this.charge;
    }

    /**
     * Gets the amount of mana left available to use before the next tick reset.
     * @return the remaining mana usable this tick
     */
    public double getRateLeft() {
        return Math.min(this.getManaRate() - this.manaUsed, this.getMana());
    }

    @Override
    public boolean consumeMana(double manaCost, boolean bypassRate) {
        if (this.charge >= manaCost) {
            this.charge -= manaCost;
            return true;
        }

        if (bypassRate) return this.source.consumeMana(manaCost, true);

        double rate = this.getManaRate();
        if (manaCost + manaUsed > rate)
            return false;

        manaCost -= this.charge;
        this.charge = 0;

        this.manaUsed += manaCost;
        return this.source.consumeMana(manaCost, false);
    }

    @Override
    public void setMana(double mana) {
        this.source.setMana(mana);
    }

    @Override
    public double getMana() {
        return this.source.getMana();
    }

    @Override
    public double getMaximumMana() {
        return this.source.getMaximumMana();
    }

    @Override
    public double getManaRate() {
        return this.source.getManaRate();
    }
}
